#!/bin/bash


# --- using: Clang++ 2.8 or LLVM-GCC 4.2 or GCC 4 ---


# make an empty directory to build in and go to it

mkdir obj
rm obj/*
cd obj


# choose compiler and set options

# if these are available, they produce an executable that is twice as fast
if which clang || which llvm-gcc-4.2
then
   if which clang
   then
      COMPILER=clang
   else
      COMPILER=llvm-gcc-4.2
   fi

   LINKER=llvm-ld
   
   OPTI="-O3 -ffast-math -emit-llvm"
   LINK_OPTIONS="-native"

# default to GCC
else
   COMPILER=gcc
   LINKER=gcc
   
   OPTI="-O3 -ffast-math"
   LINK_OPTIONS=
fi

LANG="-x c -ansi -std=iso9899:199409 -pedantic"
WARN="-Wall -Wextra -Wcast-align -Wwrite-strings -Wpointer-arith -Wredundant-decls -Wdisabled-optimization"
ARCH="-mfpmath=sse -msse"

COMPILE_OPTIONS="-c $LANG $OPTI $ARCH $WARN -Isrc"


# compile and link

echo
$COMPILER --version

$COMPILER $COMPILE_OPTIONS ../src/*.c

$LINKER $LINK_OPTIONS -o minilight -lm *.o


# move executable and return from build directory

mv minilight ..
cd ..
rm obj/*


exit
