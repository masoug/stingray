#!/bin/bash


# --- using: LLVM-GCC 4.2 or GCC 4 ---


# make an empty directory to build in, and go to it

mkdir obj
rm obj/*
cd obj


# choose compiler and set options

# if llvm-gcc is available:
COMPILER=/Developer/usr/bin/llvm-gcc
LINKER=/Developer/usr/bin/llvm-gcc
OPTI="-O4 -ffast-math"
# or use gcc:
#COMPILER=gcc
#LINKER=gcc
#OPTI="-O3 -ffast-math"

LANG="-x c -ansi -std=iso9899:199409 -pedantic"
WARN="-Wall -Wextra -Wcast-align -Wwrite-strings -Wpointer-arith -Wredundant-decls -Wdisabled-optimization"
CPU="-arch i386"
ARCH="-Xarch_i386 -mfpmath=sse -Xarch_i386 -msse"

COMPILE_OPTIONS="-c $LANG $OPTI $CPU $ARCH $WARN -Isrc"
LINK_OPTIONS=$CPU


# compile and link

echo
$COMPILER --version

$COMPILER $COMPILE_OPTIONS ../src/*.c

$LINKER $LINK_OPTIONS -o minilight *.o


# move executable and return from build directory

mv minilight ..
cd ..
rm obj/*


exit
