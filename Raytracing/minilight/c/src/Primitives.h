/*------------------------------------------------------------------------------

   MiniLight C : minimal global illumination renderer
   Harrison Ainsworth / HXA7241 : 2009, 2011

   http://www.hxa.name/minilight/

------------------------------------------------------------------------------*/


#ifndef Primitives_h
#define Primitives_h


#include <float.h>




/* types -------------------------------------------------------------------- */

typedef  signed   int    bool;

/*typedef  signed   char   byte;*/
typedef  unsigned char   ubyte;

/*typedef  signed   short  word;*/
/*typedef  unsigned short  uword;*/

typedef  signed   int    dword;
typedef  unsigned int    udword;




/* constants ---------------------------------------------------------------- */

#define false  0
#define true   1


/*#define FLOAT_MIN_POS ((float)(FLT_MIN))*/
/*#define FLOAT_MIN_NEG ((float)(-FLT_MAX))*/
#define FLOAT_MAX     ((float)(FLT_MAX))




#endif
