/*------------------------------------------------------------------------------

   MiniLight C : minimal global illumination renderer
   Harrison Ainsworth / HXA7241 : 2009, 2011

   http://www.hxa.name/minilight/

------------------------------------------------------------------------------*/


#include <math.h>
#include <stdlib.h>

#include "Exceptions.h"

#include "Image.h"




/* constants ---------------------------------------------------------------- */

/* image file comment */
static const char MINILIGHT_URI[] = "http://www.hxa.name/minilight/";

/* guess of average screen maximum brightness */
static const float DISPLAY_LUMINANCE_MAX = 200.0f;

/* ITU-R BT.709 standard RGB luminance weighting */
static const Vector3f RGB_LUMINANCE = {{ 0.2126f, 0.7152f, 0.0722f }};

/* ITU-R BT.709 standard gamma */
static const float GAMMA_ENCODE = 0.45f;




/* implementation ----------------------------------------------------------- */

/**
 * Convert FP RGB into 32bit RGBE format.
 */
static udword toRgbe
(
   const Vector3f* pRgbIn
)
{
   udword rgbe = 0;

   const Vector3f rgb  = Vector3fClamped( pRgbIn, &Vector3fZERO, pRgbIn );
   const float*   aRgb = rgb.xyz;
   const float    rgbLargest = (aRgb[0] >= aRgb[1]) ? (aRgb[0] >= aRgb[2] ?
      aRgb[0] : aRgb[2]) : (aRgb[1] >= aRgb[2] ? aRgb[1] : aRgb[2]);

   if( rgbLargest >= 1e-9f )
   {
      int   exponentLargest = 0;
      float mantissaLargest = (float)frexp( rgbLargest, &exponentLargest );

      /* has been needed in the past, sadly...
      if( 1.0f == mantissaLargest  )
      {
         mantissaLargest = 0.5f;
         exponentLargest++;
      }*/

      const float amount = mantissaLargest * 256.0f / rgbLargest;

      int i;
      for( i = 3;  i-- > 0; )
      {
         rgbe |= (udword)floor( aRgb[i] * amount ) << ((3 - i) * 8);
      }
      rgbe |= (udword)(exponentLargest + 128);
   }

   return rgbe;
}


/**
 * Calculate tone-mapping scaling factor.
 */
static float calculateToneMapping
(
   const Image* pI,
   float        divider
)
{
   /* calculate log mean luminance of scene,
      as an estimate of world-adaptation luminance */
   float logMeanLuminance = 1e-4f;
   {
      float sumOfLogs = 0.0f;
      dword i;
      for( i = (pI->width * pI->height);  i-- > 0; )
      {
         const float Y = Vector3fDot( &pI->aPixels[i], &RGB_LUMINANCE ) *
            divider;
         /* clamp luminance to a perceptual minimum */
         sumOfLogs += (float)log10( Y > 1e-4f ? Y : 1e-4f );
      }

      logMeanLuminance = (float)pow( 10.0f, sumOfLogs /
         (float)(pI->width * pI->height) );
   }

   /* make scale-factor from:
      ratio of minimum visible differences in luminance, in display-adapted
      and world-adapted perception (discluding the constant that cancelled),
      divided by display max to yield a [0,1] range */
   {
      const float a = 1.219f + (float)pow( DISPLAY_LUMINANCE_MAX * 0.25f,
         0.4f );
      const float b = 1.219f + (float)pow( logMeanLuminance, 0.4f );

      return (float)pow( a / b, 2.5f ) / DISPLAY_LUMINANCE_MAX;
   }
}


static void ImageFormattedRgbe
(
   const Image* pI,
   const float  divider,
   jmp_buf      jmpBuf,
   FILE*        pOut_o
)
{
   /* write Radiance RGBE format */

   /* write header */
   {
      /* write ID */
      throwWriteExceptions( pOut_o, jmpBuf,
         fprintf( pOut_o, "#?RADIANCE\n" ) );

      /* write other header things */
      throwWriteExceptions( pOut_o, jmpBuf,
         fprintf( pOut_o, "FORMAT=32-bit_rgbe\n" ) );
      throwWriteExceptions( pOut_o, jmpBuf,
         fprintf( pOut_o, "SOFTWARE=%s\n\n", MINILIGHT_URI ) );

      /* write width, height */
      throwWriteExceptions( pOut_o, jmpBuf,
         fprintf( pOut_o, "-Y %i +X %i\n", pI->height, pI->width ) );
   }

   /* write pixels */
   {
      dword i, b;
      for( i = 0;  i < (pI->width * pI->height);  ++i )
      {
         const Vector3f pd   = Vector3fMulF( &pI->aPixels[i], divider );
         const udword   rgbe = toRgbe( &pd );

         /* write rgbe bytes */
         for( b = 4;  b-- > 0; )
         {
            throwWriteExceptions( pOut_o, jmpBuf,
               fprintf( pOut_o, "%c", (ubyte)((rgbe >> (b * 8)) & 0xFFu) ) );
         }
      }
   }
}


static void ImageFormattedPpm
(
   const Image* pI,
   const float  divider,
   jmp_buf      jmpBuf,
   FILE*        pOut_o
)
{
   const float tonemapScaling = calculateToneMapping( pI, divider );

   /* write PPM P6 format */

   /* write header */
   {
      /* write ID and comment */
      throwWriteExceptions( pOut_o, jmpBuf,
         fprintf( pOut_o, "P6\n# %s\n\n", MINILIGHT_URI ) );

      /* write width, height, maxval */
      throwWriteExceptions( pOut_o, jmpBuf,
         fprintf( pOut_o, "%i %i\n%i\n", pI->width, pI->height, 255 ) );
   }

   /* write pixels */
   {
      dword i, c;
      for( i = 0;  i < (pI->width * pI->height);  ++i )
      {
         for( c = 0;  c < 3;  ++c )
         {
            /* tonemap */
            const float mapped = pI->aPixels[i].xyz[c] * divider *
               tonemapScaling;

            /* gamma encode */
            const float gammaed = (float)pow( (mapped > 0.0f ? mapped : 0.0f),
               GAMMA_ENCODE );

            /* quantize */
            const float quantized = (float)floor( (gammaed * 255.0f) + 0.5f );

            /* output as byte */
            throwWriteExceptions( pOut_o, jmpBuf, fprintf( pOut_o, "%c",
               (ubyte)(quantized <= 255.0f ? quantized : 255.0f) ) );
         }
      }
   }
}




/* initialisation ----------------------------------------------------------- */

Image* ImageConstruct
(
   FILE*   pIn,
   jmp_buf jmpBuf,
   bool    isHdri
)
{
   Image* pI = (Image*)throwAllocExceptions( jmpBuf,
      calloc( 1, sizeof(Image) ) );

   /* read width and height */
   throwReadExceptions( pIn, jmpBuf, 2,
      fscanf( pIn, "%i %i", &pI->width, &pI->height ) );

   /* condition width and height */
   pI->width  = pI->width  < 1 ? 1 :
      (pI->width  > IMAGE_DIM_MAX ? IMAGE_DIM_MAX : pI->width );
   pI->height = pI->height < 1 ? 1 :
      (pI->height > IMAGE_DIM_MAX ? IMAGE_DIM_MAX : pI->height);

   /* allocate pixels */
   pI->aPixels = (Vector3f*)throwAllocExceptions( jmpBuf,
      calloc( pI->width * pI->height, sizeof(Vector3f) ) );

   pI->isHdri = isHdri;

   return pI;
}


void ImageDestruct
(
   Image* pI
)
{
   /* free pixels */
   free( pI->aPixels );

   free( pI );
}




/* commands ----------------------------------------------------------------- */

void ImageAddToPixel
(
   Image*          pI,
   dword           x,
   dword           y,
   const Vector3f* pRadiance
)
{
   /* only inside image bounds */
   if( (x >= 0) & (x < pI->width) & (y >= 0) & (y < pI->height) )
   {
      const dword index = x + ((pI->height - 1 - y) * pI->width);
      pI->aPixels[index] = Vector3fAdd( &pI->aPixels[index], pRadiance );
   }
}




/* queries ------------------------------------------------------------------ */

void ImageFormatted
(
   const Image* pI,
   dword        iteration,
   jmp_buf      jmpBuf,
   FILE*        pOut_o
)
{
   /* make pixel value accumulation divider */
   const float divider = 1.0f / (float)((iteration > 0 ? iteration : 0) + 1);

   if( pI->isHdri )
   {
      ImageFormattedRgbe( pI, divider, jmpBuf, pOut_o );
   }
   else
   {
      ImageFormattedPpm( pI, divider, jmpBuf, pOut_o );
   }
}
