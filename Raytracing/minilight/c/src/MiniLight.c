/*------------------------------------------------------------------------------

   MiniLight C : minimal global illumination renderer
   Harrison Ainsworth / HXA7241 : 2009, 2011

   http://www.hxa.name/minilight/

------------------------------------------------------------------------------*/


#include <math.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*#include <time.h>*/

#include "Primitives.h"
#include "Exceptions.h"
#include "RandomMwc.h"
#include "Image.h"
#include "Scene.h"
#include "Camera.h"




/**
 * Control-module and entry point.<br/><br/>
 *
 * Handles command-line UI, and runs the main progressive-refinement render
 * loop.<br/><br/>
 *
 * Supply a model file pathname as the command-line argument. Or -? for help.
 */


/* user messages ------------------------------------------------------------ */

/* elements */
static const char TITLE[]  = "MiniLight 1.5.3 C";
static const char AUTHOR[] = "Harrison Ainsworth / HXA7241 : 2009, 2011";
static const char URL[]    = "http://www.hxa.name/minilight/";
static const char DATE[]   = "2011-01-30";
static const char LINE[]   =
"----------------------------------------------------------------------\n";
static const char DESCRIPTION[] =
"MiniLight is a minimal global illumination renderer.";
static const char USAGE[] =
"usage:\n"
"  minilight modelFilePathName\n"
"\n"
"The model text file format is:\n"
"  #MiniLight\n"
"\n"
"  iterations\n"
"\n"
"  imagewidth imageheight\n"
"  viewposition viewdirection viewangle\n"
"\n"
"  skyemission groundreflection\n"
"\n"
"  vertex0 vertex1 vertex2 reflectivity emitivity\n"
"  vertex0 vertex1 vertex2 reflectivity emitivity\n"
"  ...\n"
"\n"
"- where iterations and image values are integers, viewangle is a real,\n"
"and all other values are three parenthised reals. The file must end\n"
"with a newline. E.g.:\n";
static const char EXAMPLE[] =
"  #MiniLight\n"
"\n"
"  100\n"
"\n"
"  200 150\n"
"  (0 0.75 -2) (0 0 1) 45\n"
"\n"
"  (3626 5572 5802) (0.1 0.09 0.07)\n"
"\n"
"  (0 0 0) (0 1 0) (1 1 0)  (0.7 0.7 0.7) (0 0 0)\n"
"\n";

/* templates */
static const char BANNER_MESSAGE[] = "\n  %s - %s\n\n";
static const char HELP_MESSAGE[]   =
   "\n%s  %s\n\n  %s\n  %s\n\n  %s\n%s\n%s\n\n%s%s";




/* constants ---------------------------------------------------------------- */

static const char MODEL_FORMAT_ID[] = "#MiniLight";

#define ERROR_FORMAT_UNREC 1
#define ERROR_FILE         128




/* ctrl-c handling ---------------------------------------------------------- */

static jmp_buf jmpBuf_g;

static char*  sImageFilePathname_g = 0;
static Image* pImage_g             = 0;
static dword  frameNo_g            = 0;


void saveImage
(
   int arg
)
{
   /* open image file */
   FILE* pImageFile = fopen( sImageFilePathname_g, "wb" );
   throwExceptions( jmpBuf_g, !pImageFile, ERROR_FILE );

   /* write image frame to file */
   ImageFormatted( pImage_g, frameNo_g - 1, jmpBuf_g, pImageFile );

   throwExceptions( jmpBuf_g, (EOF == fclose( pImageFile )), ERROR_FILE );

   /* exit if called (asynchronously) by an interrupt */
   if( arg )
   {
      printf( "\ninterrupted\n" );
      exit( EXIT_SUCCESS );
   }
}




/* implementation ----------------------------------------------------------- */

static void makeRenderingObjects
(
   char*         argv[],
   char**        psImageFilePathname_o,
   dword*        pIterations_o,
   Image**       ppImage_o,
   Camera*       pCamera_o,
   const Scene** ppScene_o,
   RandomMwc*    pRandom_o
)
{
   FILE* pModelFile;

   const bool isHdri = !strcmp( argv[1], "-hdri" );

   /* get/make file names */
   const char* sModelFilePathname = argv[isHdri ? 2 : 1];
   *psImageFilePathname_o = (char*)throwAllocExceptions( jmpBuf_g,
      calloc( strlen(sModelFilePathname) + 6, sizeof(char) ) );
   strcpy( *psImageFilePathname_o, sModelFilePathname );
   strcat( *psImageFilePathname_o, isHdri ? ".rgbe" : ".ppm" );

   /* open model file */
   pModelFile = fopen( sModelFilePathname, "r" );
   throwExceptions( jmpBuf_g, !pModelFile, ERROR_FILE );

   /* check model file format identifier at start of first line */
   {
      long p;
      /* read chars until a mismatch */
      throwReadExceptions( pModelFile, jmpBuf_g, 0,
         fscanf( pModelFile, MODEL_FORMAT_ID ) );
      p = ftell( pModelFile );
      throwExceptions( jmpBuf_g, (-1L == p), ERROR_FILE );
      /* check if all chars were read */
      throwExceptions( jmpBuf_g,
         ((long)strlen( MODEL_FORMAT_ID ) != p), ERROR_FORMAT_UNREC );
   }

   /* read and condition frame iterations */
   *pIterations_o = 0;
   throwReadExceptions( pModelFile, jmpBuf_g, 1,
      fscanf( pModelFile, "%i", pIterations_o ) );
   *pIterations_o = *pIterations_o < 0 ? 0 : *pIterations_o;

   /* create main rendering objects, from model file */
   *ppImage_o = ImageConstruct( pModelFile, jmpBuf_g, isHdri );
   *pCamera_o = CameraCreate( pModelFile, jmpBuf_g );
   *ppScene_o = SceneConstruct( pModelFile, jmpBuf_g,
      &CameraEyePoint( pCamera_o ) );

   /* close model file */
   throwExceptions( jmpBuf_g, (EOF == fclose( pModelFile )), ERROR_FILE );

   /* make deterministic (or not) random */
   *pRandom_o = RandomMwcCreate( 0 /*(udword)time( 0 )*/ );
}


static void renderProgressively
(
   const dword   iterations,
   const Camera* pCamera,
   const Scene*  pScene,
   RandomMwc*    pRandom,
   Image*        pImage_o
)
{
   /* do progressive refinement render loop */
   dword frameNo;
   for( frameNo = 1;  frameNo <= iterations;  ++frameNo )
   {
      /* render a frame */
      CameraFrame( pCamera, pScene, pRandom, pImage_o );
      frameNo_g = frameNo;

      /* save image at twice error-halving rate, and at start and end */
      if( ((frameNo & (frameNo - 1)) == 0) | (iterations == frameNo) )
      {
         saveImage( 0 );
      }

      /* display latest iteration number */
      printf( "\riteration: %i", frameNo );
      fflush( stdout );
   }
}


/*void destruct()
{
   SceneDestruct( (Scene*)pScene_g );
   ImageDestruct( pImage_g );
   fclose( pModelFile_g );
   free( sImageFilePathname_g );
}*/




/* entry point ************************************************************** */

int main
(
   int   argc,
   char* argv[]
)
{
   int returnValue = EXIT_FAILURE;

   const char* sException = 0;

   /* extract thrown value */
   switch( setjmp( jmpBuf_g ) )
   {
      case 0                  : sException = 0;                           break;
      case ERROR_READ_INVAL   : sException = "invalid model syntax";      break;
      case ERROR_READ_TRUNC   : sException = "truncated model file";      break;
      case ERROR_READ_IO      : sException = "i/o read error";            break;
      case ERROR_WRITE_IO     : sException = "i/o write error";           break;
      case ERROR_FORMAT_UNREC : sException = "unrecognised model format"; break;
      case ERROR_FILE         : sException = "file error";                break;
      case ERROR_ALLOC        : sException = "storage allocation error";  break;
      default                 : sException = "(unspecified error)";       break;
   }
   /* try */
   if( !sException )
   {
      /* check for help request */
      if( (argc <= 1) || !strcmp(argv[1], "-?") || !strcmp(argv[1], "--help") )
      {
         printf( HELP_MESSAGE, LINE, TITLE, AUTHOR, URL, DATE, LINE,
            DESCRIPTION, USAGE, EXAMPLE );
      }
      /* execute */
      else
      {
         dword        iterations;
         Camera       camera;
         const Scene* pScene;
         RandomMwc    random;

         printf( BANNER_MESSAGE, TITLE, URL );

         /* don't bother clearing up, since everything will be anyway */
         /*atexit( destruct );*/

         makeRenderingObjects( argv, &sImageFilePathname_g, &iterations,
            &pImage_g, &camera, &pScene, &random );

         /* setup ctrl-c/image-saving handler */
         signal( SIGINT, saveImage );
         /*throwExceptions( jmpBuf_g,
            (signal( SIGINT, saveImage ) == SIG_ERR), ERROR_UNSPECIFIED );*/

         renderProgressively( iterations, &camera, pScene, &random, pImage_g );

         printf( "\nfinished\n" );
      }

      returnValue = EXIT_SUCCESS;
   }
   /* catch everything */
   else
   {
      /* print exception message */
      printf( "\n*** execution failed:  %s\n", sException );

      returnValue = EXIT_FAILURE;
   }

   return returnValue;
}
