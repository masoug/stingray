#ifndef _RAYBOX_H_
#define _RAYBOX_H_

typedef unsigned int udword;

//! Integer representation of a floating-point value.
#define IR(x)	((udword&)x)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	A method to compute a ray-AABB intersection.
 *	Original code by Andrew Woo, from "Graphics Gems", Academic Press, 1990
 *	Optimized code by Pierre Terdiman, 2000 (~20-30% faster on my Celeron 500)
 *	Epsilon value added by Klaus Hartmann. (discarding it saves a few cycles only)
 *
 *	Hence this version is faster as well as more robust than the original one.
 *
 *	Should work provided:
 *	1) the integer representation of 0.0f is 0x00000000
 *	2) the sign bit of the float is the most significant one
 *
 *	Report bugs: p.terdiman@codercorner.com
 *
 *	\param		aabb		[in] the axis-aligned bounding box
 *	\param		origin		[in] ray origin
 *	\param		dir			[in] ray direction
 *	\param		coord		[out] impact coordinates
 *	\return		true if ray intersects AABB
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#define RAYAABB_EPSILON 0.00001f
inline
bool RayAABB( const float bounds[ 6 ], const float origin[ 3 ], const float dir[ 3 ], float coord[ 3 ] ) {
	bool Inside = true;
	
	float MaxT[ 3 ], MinB[ 3 ], MaxB[ 3 ];
	MinB[ 0 ] = bounds[ 0 ];
	MinB[ 1 ] = bounds[ 1 ];
	MinB[ 2 ] = bounds[ 2 ];
	MaxB[ 0 ] = bounds[ 3 ];
	MaxB[ 1 ] = bounds[ 4 ];
	MaxB[ 2 ] = bounds[ 5 ];

	MaxT[ 0 ] = MaxT[ 1 ] = MaxT[ 2 ] = -1.0f;

	// Find candidate planes.
	udword i;
	for(i=0;i<3;i++) {
		if( origin[ i ] < MinB[ i ] ) {
			coord[i] = MinB[i];
			Inside		= false;

			// Calculate T distances to candidate planes
			if(IR(dir[i]))	MaxT[i] = (MinB[i] - origin[i]) / dir[i];
		}
		else if(origin[i] > MaxB[i])
		{
			coord[i]	= MaxB[i];
			Inside		= false;

			// Calculate T distances to candidate planes
			if(IR(dir[i]))	MaxT[i] = (MaxB[i] - origin[i]) / dir[i];
		}
	}

	// Ray origin inside bounding box
	if(Inside)
	{
		coord[ 0 ] = origin[ 0 ];
		coord[ 1 ] = origin[ 1 ];
		coord[ 2 ] = origin[ 2 ];
		return true;
	}

	// Get largest of the maxT's for final choice of intersection
	udword WhichPlane = 0;
	if(MaxT[1] > MaxT[WhichPlane])	WhichPlane = 1;
	if(MaxT[2] > MaxT[WhichPlane])	WhichPlane = 2;

	// Check final candidate actually inside box
	if(IR(MaxT[WhichPlane])&0x80000000) return false;

	for(i=0;i<3;i++)
	{
		if(i!=WhichPlane)
		{
			coord[i] = origin[i] + MaxT[WhichPlane] * dir[i];
#ifdef RAYAABB_EPSILON
			if(coord[i] < MinB[i] - RAYAABB_EPSILON || coord[i] > MaxB[i] + RAYAABB_EPSILON)	return false;
#else
			if(coord[i] < MinB[i] || coord[i] > MaxB[i])	return false;
#endif
		}
	}
	return true;	// ray hits box
}
#endif // _RAYBOX_H_
