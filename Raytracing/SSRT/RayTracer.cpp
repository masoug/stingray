/********************************************************************************************
 ** SSRT - Shaun's Simple Ray Tracer                                                       **
 **        By Shaun Nirenstein: shaun@cs.uct.ac.za                                         **
 **                             http://people.cs.uct.ac.za/~snirenst                       **
 **                                                                                        **
 ** License agreement:                                                                     **
 ** - This source code (or compiled version thereof) CANNOT be used freely for commercial  **
 **   purposes.  If you wish to use it for such purposes, please contact the author,       **
 **   Shaun Nirenstein (shaun@cs.uct.ac.za).                                               **
 ** - For educational purposes (personal or institutional), this source code may be        **
 **   modified, distributed or extended freely, as long as this license agreement appears  **
 **   at the top of every file as is.                                                      **
 ** - Any program modified or extended from a version of SSRT (or a modified or extended   **
 **   version thereof) must acknowledge that it is SSRT derived in an accessible about box **
 **   or credit screen.                                                                    **
 **                                                                                        **
 ********************************************************************************************
 */
#include "VecMath.h"
#include "Model.h"
#include "RayTracer.h"

#include <float.h>
#include <list>

#include <windows.h>

using namespace std;

namespace Tracer {

/**
 * Create a ray tracer object, for the given model.  The output will go to
 * the
 */
RayTracer::RayTracer( Model *model, unsigned char *bitmap ) {
	m_model = model;
	m_bitmap = bitmap;
	for( int y = 0; y < m_model->m_height; y++ )
		for( int x = 0; x < m_model->m_width; x++ ) {
			bitmap[ x * 4 + y * m_model->m_width * 4     ] = ( x * 5 + y * 7 ) % 256; // B
			bitmap[ x * 4 + y * m_model->m_width * 4 + 1 ] = ( x * 3 + y * 2 ) % 256; // G
			bitmap[ x * 4 + y * m_model->m_width * 4 + 2 ] = ( x + y ) % 256; // R
			bitmap[ x * 4 + y * m_model->m_width * 4 + 3 ] = 0; // A (unused)
		}
	m_pixels = new float[ 3 * m_model->m_width * m_model->m_height ];
	m_currentLine = 0;
	m_tracing = true;
	
	// Build camera vectors	
	float aspectRatio = (float)m_model->m_width/(float)m_model->m_height;
	Utils::cross( m_uVec, m_model->m_direction, m_model->m_up );
	Utils::cross( m_vVec, m_model->m_direction, m_uVec );
	Utils::normalise( m_uVec );
	Utils::normalise( m_vVec );
	Utils::scale( m_uVec, aspectRatio );
	m_pixelSize[ 0 ] = aspectRatio / (float)m_model->m_width;
	m_pixelSize[ 1 ] = 1.0f / (float)m_model->m_width;

	// Corner
	float direction[ 3 ], halfU[ 3 ], halfV[ 3 ];
	Utils::setf3( direction, m_model->m_direction );	
	Utils::normalise( direction );
	Utils::setf3( halfU, m_uVec );
	Utils::scale( halfU, -0.5f );
	Utils::setf3( halfV, m_vVec );
	Utils::scale( halfV, -0.5f );
	Utils::add3f( m_corner, m_model->m_eye, direction ); // Corner is now in the center of the image plane
	Utils::add3f( m_corner, halfU ); // Now Corner is on the left border in the center
	Utils::add3f( m_corner, halfV ); // Now Corner is on the top left corner

	// Set up strata
	m_numberOfRandomSequences = 20;
	m_samplesPerStrata = 20; 
	m_currentSample = 0;
	m_strataSequences = new int[ m_model->m_pathDepth ];
	generateRandomSequences();

	// Light sources
	generateLightSourcePoints();

	// Sphere points (for generating random rays and such)	
	generateDiffuseRayTable();	
}

/**
 * Clear the memory used by the ray tracer.
 */
RayTracer::~RayTracer() {
	delete[] m_strataSequences;
	for( int i = 0; i < m_numberOfRandomSequences; i++ )
		delete[] m_randomSequences[ i ];
	delete[] m_randomSequences;
	delete[] m_diffuseTable;
}

/**
 * Trace a single line.
 * This calls, tracePixel, which in turn generates a ray for each stratum of the pixel
 * and traces this with traceRay.
 */
void RayTracer::traceLine() {
	// Trace a line
	m_tracing = true;
	if ( m_currentLine == m_model->m_height )
		return;

	// Trace current line	(actually, 2 lines at a time to increase coherence)
	for( int x = 0; x < m_model->m_width; x++ ) {
		float u = (float)x / (float)m_model->m_width;
		float v = (float)m_currentLine / (float)m_model->m_height;
		tracePixel( u, v, &m_pixels[ x * 3 + m_currentLine * 3 * m_model->m_width ] );
		v = (float)( m_currentLine + 1 ) / (float)m_model->m_height;		
		tracePixel( u, v, &m_pixels[ x * 3 + ( m_currentLine + 1 ) * 3 * m_model->m_width ] );
	}	

	// Finished image?
	m_currentLine += 2;
	if ( m_currentLine == m_model->m_height )
		m_tracing = false;
}

/**
 * Compute a pixel value by tracing rays through a pixel.
 */
void RayTracer::tracePixel( const float u, const float v, float pixel[ 3 ] ) {
	pixel[ 0 ] = pixel[ 1 ] = pixel[ 2 ] = 0.0f; // Zero pixel
	int uStratum, vStratum;	
	float stratumSize[ 2 ];
	stratumSize[ 0 ] = m_pixelSize[ 0 ] / m_model->m_raysPerPixel;
	stratumSize[ 1 ] = m_pixelSize[ 1 ] / m_model->m_raysPerPixel;	
	for( int i = 0; i < m_model->m_pathDepth; i++ ) 
		m_strataSequences[ i ] = rand() % m_numberOfRandomSequences;
	m_currentSample = 0;
	for( uStratum = 0; uStratum < m_model->m_raysPerPixel; uStratum++ )
		for( vStratum = 0; vStratum < m_model->m_raysPerPixel; vStratum++ ) {
			float target[ 3 ], suVec[ 3 ], svVec[ 3 ], dir[ 3 ];
			Utils::setf3( suVec, m_uVec );
			Utils::setf3( svVec, m_vVec );		
			Utils::scale( suVec, u + ( (float)uStratum + RAND01 ) * stratumSize[ 0 ] );
			Utils::scale( svVec, v + ( (float)vStratum + RAND01 ) * stratumSize[ 1 ] );
			Utils::add3f( target, m_corner, suVec );
			Utils::add3f( target, svVec );
			Utils::sub3f( dir, target, m_model->m_eye );
			Utils::normalise( dir );			
			traceRay( m_model->m_eye, dir, pixel );
			m_currentSample++;
		}
	Utils::scale( pixel, 1.0f / (float)m_model->m_raysPerPixel );
}

/**
 * Path tracing loop.
 * 
 * Consider a segment in the path, with a source and a target.
 * All that is known on entry is the source information.
 * The rayIntersect routine, brings out information about the
 * target.  A contribution of the target is then computed and
 * accumulated (into 'pixel').
 *
 * Input into loop:
 * - Source point : point at source
 * - Source direction : ray direction from which to gather intensity/radiance
 * - Source normal : normal at source  
 * - Source colour : how this gathered intensity/radiance will be transmitted
 * - Cosine Accumulation : dot product between normal and emitted ray.  This 
 *                         gives the Lambertian contribution. It is accumulated
 *                         for each bounce, since the light incident at an indirect
 *                         light source also follows the lambertian rule.
 *                         NOTE: This has been made redudant with the cosine-sampling
 *                               used.  It is now used to handle the 2x factor 
 *                               introduced by the importance sampling.
 * 
 * Special values are set on entry, since the initial source is not a surface point.
 * - Source point : eye point
 * - Source dir : ray direction associated with pixel (sub)sample
 * - Source normal : same as direction, so that dot product returns 1 (i.e., it's nullified)
 * - Source colour : { 1, 1, 1 } since we want the light emitted
 * - Cosine Accumulation : 1 since no bounces have ocurred yet
 */
void RayTracer::traceRay( const float origin[ 3 ], const float dir[ 3 ], float pixel[ 3 ] ) {
	// Initial conditions
	float sourcePoint[ 3 ], sourceDir[ 3 ], sourceNormal[ 3 ];
	Utils::setf3( sourcePoint, origin );	
	Utils::setf3( sourceDir, dir );	
	Utils::setf3( sourceNormal, dir );
	Utils::normalise( sourceNormal );
	float sourceColour[ 3 ] = { 1.0f, 1.0f, 1.0f }, cosineAccumulation = 1.0f;

	// Trace path
	for( int depth = 0; depth < m_model->m_pathDepth; depth++ ) {		
		int hitObjectIndex;
		float targetIntersectionPoint[ 3 ], targetNormal[ 3 ];
		if ( m_model->rayIntersect( sourcePoint, sourceDir, hitObjectIndex, targetIntersectionPoint, targetNormal ) ) {
			float direct[ 3 ] = {0}; // Direct contribution at target			
			Object *hitObject = &m_model->m_objects[ hitObjectIndex ];
			bool isLightSource = hitObject->m_lightSource;
			if ( isLightSource )
				Utils::setf3( direct, hitObject->m_colour );
			else
				computeDirectContribution( targetIntersectionPoint, targetNormal, hitObjectIndex, direct );			
			// The indirect colour that is transmitted FROM the source point:
			float transmittedColour[ 3 ];
			Utils::mul3f( transmittedColour, sourceColour, direct ); // Apply source reflectance/absorption
	
			// Finally, scale the transmitted colour by the accumulated cosine factor			
			Utils::scale( transmittedColour, cosineAccumulation );

			// Add contribution			
			Utils::add3f( pixel, transmittedColour );			
			
			// Stop recursion, since we assume light sources don't "reflect" light
			if ( isLightSource )
				break;

			if ( depth + 1 != m_model->m_pathDepth ) { // Generate another path segment?
				// Prepare next ray
				Utils::setf3( sourcePoint, targetIntersectionPoint );				
				Utils::setf3( sourceNormal, targetNormal );
				// Generate random direction to sample in
				float e = RAND01;				
				if ( e < hitObject->m_diffuse ) {
					getDiffuseRay( targetNormal, depth, sourceDir );					
				} else if ( e < hitObject->m_diffuse + hitObject->m_specular ) {
					float targetReflection[ 3 ];
					Utils::reflect( sourceDir, targetNormal, targetReflection );
					if ( hitObject->m_perfectSpecular )
						Utils::setf3( sourceDir, targetReflection );
					else
						getSpecularRay( targetReflection, hitObject->m_specularIndex, sourceDir ); // TODO: (N + 2)/2PI scaling for reflection
					if ( Utils::dot( sourceDir, targetNormal ) <= 0 )
						break; // The reflected direction goes behind the object
				} else if ( e < hitObject->m_diffuse + hitObject->m_specular + hitObject->m_transmission ) {
					float targetRefraction[ 3 ];
					float lv[ 3 ];
					Utils::neg3f( lv, sourceDir );
					Utils::refract( lv, targetNormal, 1.0f, hitObject->m_indexOfRefraction, targetRefraction );
					if ( hitObject->m_perfectSpecular )
						Utils::setf3( sourceDir, targetRefraction);
					if ( Utils::dot( sourceDir, targetNormal ) >= 0 )
						break; // The refracted direction goes in front of the object
				} // Otherwise it is absorbed
				if ( ! ( ( hitObject->m_specular > 0 || hitObject->m_transmission > 0 )&& hitObject->m_perfectSpecular ) )
					cosineAccumulation *= 0.5f; //Utils::dot( targetNormal, sourceDir ); // Negate this, since this is from the perspective of the point that is hit								
				// Colour transmitted by new sourcePoint				
				Utils::mul3f( sourceColour, m_model->m_objects[ hitObjectIndex ].m_colour, sourceColour );
			}					
		} else
			break;
	} 
}

/**
 * Direct contribution:
 * 1. Find a point on the light source (the point sources built from area light sources)
 * 2. See if intersectionPoint can see it
 * 3. If it can, then add direct contribution
 * 4. Average several samples to get accurate direct contribution	
*/
void RayTracer::computeDirectContribution( const float intersectionPoint[ 3 ], const float normal[ 3 ], const int hitObjectIndex, float direct[ 3 ] ) {
	int numberOfSamplesToTest = (int)( (float)m_numLights * m_model->m_lightSampleRatio );
	for( int i = 0; i < numberOfSamplesToTest; i++ ) {
		// Compute light vector
		PointLight *light;
		if ( numberOfSamplesToTest == m_numLights )
			light = &m_lights[ i ]; // Make sure that every light source is visited
		else
			light = &m_lights[ rand() % m_numLights ]; // Choose a random light source
		float lightVector[ 3 ];
		Utils::sub3f( lightVector, light->m_point, intersectionPoint );
		
		// Can point see this light sample?
		// 1. Is the point behind the area light source polygon?
		float planeDistance = Utils::dot( light->m_plane, intersectionPoint ) - light->m_plane[ 3 ];
		
		// 2. Is the light source blocked from view (or vice versa)
		if ( planeDistance > 0 && m_model->testShadowRay( intersectionPoint, lightVector ) ) {
			Utils::normalise( lightVector );
			float diffuseContribution = Utils::dot( normal, lightVector ); 
			if ( diffuseContribution > 0 ) {
				float localColour[ 3 ];
				// Compute local diffuse lighting
				Object *obj = &m_model->m_objects[ hitObjectIndex ];
				Utils::mul3f( localColour, m_model->m_objects[ light->m_objectIndex ].m_colour, obj->m_colour );
				float emissionDistribution = -Utils::dot( light->m_plane, lightVector );								

				// Put it all together				
				Utils::scale( localColour, emissionDistribution * diffuseContribution * obj->m_diffuse );
				Utils::add3f( direct, localColour );
			}
		}			
	}
	Utils::scale( direct, 1.0f / (float)numberOfSamplesToTest );
}

/**
 * Generate random points on the light sources.  This creates points in
 * such a manner, that larger light sources get proportionally more
 * points.
 */
void RayTracer::generateLightSourcePoints() {
	// Find area of smallest lightsource triangle and total light source area
	float minArea = FLT_MAX, totalArea = 0;
	for( int i = 0; i < m_model->m_numObjects; i++ ) {
		if ( m_model->m_objects[ i ].m_lightSource ) {
			float tri[ 3 ][ 3 ];
			for( int j = 0; j < m_model->m_objects[ i ].m_numIndices / 3; j++ ) {
				m_model->getTriangle( i, j, tri );
				float triArea = Utils::triangleArea( tri );
				if ( triArea < minArea )
					minArea = triArea;
				totalArea += triArea;
			}
		}
	}

	// Temporary structure for light sources and their object associations
	list<PointLight*> lightList;	

	const int lightSamples = m_model->m_lightSamples;
	// Generate points for light source
	for( int i = 0; i < m_model->m_numObjects; i++ ) {
		if ( m_model->m_objects[ i ].m_lightSource ) {
			float tri[ 3 ][ 3 ];
			Object *obj = &m_model->m_objects[ i ];
			for( int j = 0; j < obj->m_numIndices / 3; j++ ) {
				m_model->getTriangle( i, j, tri );
				float triArea = Utils::triangleArea( tri );
				int numberOfSamples = (int) ( ( triArea / minArea ) * (float)lightSamples + 0.01f ); // So the smallest triangle gets lightSamples samples, one that's n times bigger gets n*lightSamples, etc. (0.01f is just an epsilon)
				for( int k = 0; k < numberOfSamples; k++ ) {
					PointLight *pl = new PointLight();
					Utils::generatePointOnTriangle( tri, pl->m_point );
					pl->m_objectIndex = i;
					pl->m_triangleIndex = j;
					Utils::setf3( pl->m_plane, &obj->m_normals[ j * 3 ] );
					pl->m_plane[ 3 ] = obj->m_planeConstants[ j ];
					lightList.push_back( pl );
				}
			}
		}
	}
	
	// Make an array copy of the point light souces
	m_numLights = (int)lightList.size();
	m_lights = new PointLight[ m_numLights ];
	list<PointLight*>::iterator it = lightList.begin();
	for( int i = 0; i < m_numLights; i++, it++ ) {
		m_lights[ i ] = **it; // Shallow copy
		delete *it; // Delete list copy
	}
}

/**
 * The diffuse ray table is used to store a precomputed cosine weighted sample
 * distribution (built once, for efficiency).
 */
void RayTracer::generateDiffuseRayTable() {
	m_diffuseTable = new float[ m_samplesPerStrata * m_model->m_raysPerPixel * m_model->m_raysPerPixel * 3 ];
	int i = 0;
	for( int stratum = 0; stratum < m_samplesPerStrata; stratum++ ) 
		for( int uStrata = 0; uStrata < m_model->m_raysPerPixel; uStrata++ )
			for( int vStrata = 0; vStrata < m_model->m_raysPerPixel; vStrata++ ) {
				float u = ( (float)uStrata + RAND01 ) / (float)m_model->m_raysPerPixel;
				float v = ( (float)vStrata + RAND01 ) / (float)m_model->m_raysPerPixel;
				float theta = 2 * 3.1415f * u; // Azimuth
				float phi = acos( sqrt( v ) );
				m_diffuseTable[ i * 3     ] = cos( theta ) * sin( phi );
				m_diffuseTable[ i * 3 + 1 ] = sin( theta ) * sin( phi );
				m_diffuseTable[ i * 3 + 2 ] = cos( phi );
				i++;
			}
}

/**
 * Generate random sequences of numbers.  This is used for the
 * diffuse sample stratification.
 */
void RayTracer::generateRandomSequences() {
	m_randomSequences = new int*[ m_numberOfRandomSequences ];
	int numRays = m_model->m_raysPerPixel * m_model->m_raysPerPixel;
	for( int i = 0; i < m_numberOfRandomSequences; i++ ) {
		m_randomSequences[ i ] = new int[ numRays ];
		for( int j = 0; j <numRays; j++ )
			m_randomSequences[ i ][ j ] = j;
		for( int j = 0; j < numRays; j++ ) { // Swap around sequence randomly
			int i1 = rand() % numRays, i2 = rand() % numRays;
			int temp = m_randomSequences[ i ][ i1 ];
			m_randomSequences[ i ][ i1 ] = m_randomSequences[ i ][ i2 ];
			m_randomSequences[ i ][ i2 ] = temp;
		}
	}
}

/**
 * Get a diffuse ray (cosine weighted distribution).  The the ray is rotated so that
 * the distribution lookup will occur relative to the normal.
 */
void RayTracer::getDiffuseRay( const float normal[ 3 ], const int depth, float ray[ 3 ] ) {
	int index = m_randomSequences[ m_strataSequences[ depth ] ][ m_currentSample ];//rand() % m_model->m_raysPerPixel * m_model->m_raysPerPixel;
	int stratum = ( rand() % m_samplesPerStrata ) * m_model->m_raysPerPixel * m_model->m_raysPerPixel;
	float ray1[ 3 ], ray2[ 3 ];
	Utils::setf3( ray1, &m_diffuseTable[ ( index + stratum ) * 3 ] );			
	// Rotate ray1 to distribution of normal vector	(output: ray)
	float el = -acos( normal[ 2 ] );
	float az = -atan2( normal[ 1 ], normal[ 0 ] );	
	// Y Rot
	ray2[ 0 ] = cos( el ) * ray1[ 0 ] - sin( el ) * ray1[ 2 ]; // TODO: Factor out sin and cos
	ray2[ 1 ] = ray1[ 1 ];
	ray2[ 2 ] = sin( el ) * ray1[ 0 ] + cos( el ) * ray1[ 2 ];
	// Z Rot	
	ray[ 0 ] = cos( az ) * ray2[ 0 ] + sin( az ) * ray2[ 1 ];
	ray[ 1 ] = -sin( az ) * ray2[ 0 ] + cos( az ) * ray2[ 1 ];		
	ray[ 2 ] = ray2[ 2 ];	
	//Utils::normalise( ray );	
}

/**
 * Generate a specular ray, around cosine lobe relative to the 'reflection' direction, based on
 * the specular index. This is not precomputed, since each imperfect specular surface would have
 * to have it's own samples due to the varying specular index.
 */
void RayTracer::getSpecularRay( const float reflection[ 3 ], const float specularIndex, float ray[ 3 ] ) {
	float ray1[ 3 ], ray2[ 3 ];
	// Set ray1 as the distributed ray wrt a (0, 0, 1) focus
	float theta = 2 * 3.1415f * RAND01; // Azimuth
	float phi = acos( sqrt( pow( RAND01, 1.0f / ( specularIndex + 1 ) ) ) ); // Elevation N = 0 is just a uniform sphere distribution
	ray1[ 0 ] = cos( theta ) * sin( phi );
	ray1[ 1 ] = sin( theta ) * sin( phi );
	ray1[ 2 ] = cos( phi );		
	// Rotate ray1 to distribution of normal vector	(output: ray)
	float el = -acos( reflection[ 2 ] );
	float az = -atan2( reflection[ 1 ], reflection[ 0 ] );	
	// Y Rot
	ray2[ 0 ] = cos( el ) * ray1[ 0 ] - sin( el ) * ray1[ 2 ]; // TODO: Factor out sin and cos
	ray2[ 1 ] = ray1[ 1 ];
	ray2[ 2 ] = sin( el ) * ray1[ 0 ] + cos( el ) * ray1[ 2 ];
	// Z Rot	
	ray[ 0 ] = cos( az ) * ray2[ 0 ] + sin( az ) * ray2[ 1 ];
	ray[ 1 ] = -sin( az ) * ray2[ 0 ] + cos( az ) * ray2[ 1 ];		
	ray[ 2 ] = ray2[ 2 ];	
	Utils::normalise( ray );
	float dp = Utils::dot( reflection, ray );	
}

/**
 * Update the bitmap with the data that's been computed so far.  The image is normalized and
 * this can be controlled via the 'saturation' value.
 */
void RayTracer::updateBitmap() {
	float max = 0, invMax;
	for( int i = 0; i < m_currentLine * m_model->m_width * 3; i++ )
		if ( max < m_pixels[ i ] )
			max = m_pixels[ i ];
	max = max * m_model->m_saturation;	
	invMax = 255.0f / max;
	for( int i = 0; i < m_currentLine * m_model->m_width; i++ ) {
		float pixelMax = Utils::maximum3( m_pixels[ i * 3 ], m_pixels[ i * 3 + 1 ], m_pixels[ i * 3 + 2 ] );
		float pixelMultiplier = invMax;
		if ( pixelMax > max )
			pixelMultiplier *= max / pixelMax;
		m_bitmap[ i * 4     ] = (unsigned char) ( m_pixels[ i * 3 + 2 ] * pixelMultiplier ); // B
		m_bitmap[ i * 4 + 1 ] = (unsigned char) ( m_pixels[ i * 3 + 1 ] * pixelMultiplier ); // G
		m_bitmap[ i * 4 + 2 ] = (unsigned char) ( m_pixels[ i * 3     ] * pixelMultiplier ); // R
	}
}

/** 
 * Return the progress of the ray-tracer in the range [0,1]
 */
float RayTracer::getProgress() {
	return (float)m_currentLine / (float)m_model->m_height;
}

};
