/********************************************************************************************
 ** SSRT - Shaun's Simple Ray Tracer                                                       **
 **        By Shaun Nirenstein: shaun@cs.uct.ac.za                                         **
 **                             http://people.cs.uct.ac.za/~snirenst                       **
 **                                                                                        **
 ** License agreement:                                                                     **
 ** - This source code (or compiled version thereof) CANNOT be used freely for commercial  **
 **   purposes.  If you wish to use it for such purposes, please contact the author,       **
 **   Shaun Nirenstein (shaun@cs.uct.ac.za).                                               **
 ** - For educational purposes (personal or institutional), this source code may be        **
 **   modified, distributed or extended freely, as long as this license agreement appears  **
 **   at the top of every file as is.                                                      **
 ** - Any program modified or extended from a version of SSRT (or a modified or extended   **
 **   version thereof) must acknowledge that it is SSRT derived in an accessible about box **
 **   or credit screen.                                                                    **
 **                                                                                        **
 ********************************************************************************************
 */

/****************************************** Vector maths functions ***********************************************/
#ifndef _VECMATH_H_
#define _VECMATH_H_

#include <math.h>
#include <stdlib.h>
#include <memory.h>

namespace Utils {

#define RAND01 (float) ( (float)( rand() % 10001 ) / 10000.0f )
  const double PI = 3.1415926535897932384626433832795f;

	inline
	void setf3( float *target, const float *source ) {
		memcpy( target, source, sizeof( float ) * 3 );
	}

	inline 
	void sub3f( float target[], const float source1[], const float source2[]) {
		target[ 0 ] = source1[ 0 ] - source2[ 0 ];
		target[ 1 ] = source1[ 1 ] - source2[ 1 ];
		target[ 2 ] = source1[ 2 ] - source2[ 2 ];
	}

	inline 
	void neg3f( float target[], const float source[] ) {
		target[ 0 ] = -source[ 0 ];
		target[ 1 ] = -source[ 1 ];
		target[ 2 ] = -source[ 2 ];
	}

	inline 
	void neg3f( float target[] ) {
		target[ 0 ] = -target[ 0 ];
		target[ 1 ] = -target[ 1 ];
		target[ 2 ] = -target[ 2 ];
	}

	inline 
	void sub3f( float target[], const float source[] ) {
		target[ 0 ] -= source[ 0 ];
		target[ 1 ] -= source[ 1 ];
		target[ 2 ] -= source[ 2 ];
	}

	inline 
	void add3f( float target[], const float source1[], const float source2[]) {
		target[ 0 ] = source1[ 0 ] + source2[ 0 ];
		target[ 1 ] = source1[ 1 ] + source2[ 1 ];
		target[ 2 ] = source1[ 2 ] + source2[ 2 ];
	}

	inline 
	void add3f( float target[], const float source[] ) {
		target[ 0 ] += source[ 0 ];
		target[ 1 ] += source[ 1 ];
		target[ 2 ] += source[ 2 ];
	}

	inline 
	void mul3f( float target[], const float source1[], const float source2[]) {
		target[ 0 ] = source1[ 0 ] * source2[ 0 ];
		target[ 1 ] = source1[ 1 ] * source2[ 1 ];
		target[ 2 ] = source1[ 2 ] * source2[ 2 ];
	}

	inline 
	void mul3f( float target[], const float source[] ) {
		target[ 0 ] *= source[ 0 ];
		target[ 1 ] *= source[ 1 ];
		target[ 2 ] *= source[ 2 ];
	}

	inline
	void normalise( float vector[] ) {
		float sum;
		sum =  vector[ 0 ] * vector[ 0 ];	
		sum += vector[ 1 ] * vector[ 1 ];
		sum += vector[ 2 ] * vector[ 2 ];

		sum = 1.0f/(float)sqrt( sum );
		vector[0] *= sum;
		vector[1] *= sum;
		vector[2] *= sum;
	}

	inline 
	float dot( const float a[], const float b[] ) {
		return a[ 0 ] * b[ 0 ] + a[ 1 ] * b[ 1 ] + a[ 2 ] * b[ 2 ];
	}

	/** 
	* Return the cosine of the angle between the two vectors.
	* No assumption of normalisation is made.
	*/
	inline 
	float angle( const float a[], const float b[] ) {				
		float aLen= sqrt( a[ 0 ] * a[ 0 ] + a[ 1 ] * a[ 1 ] + a[ 2 ] * a[ 2 ] );
		float bLen = sqrt( b[ 0 ] * b[ 0 ] + b[ 1 ] * b[ 1 ] + b[ 2 ] * b[ 2 ] );
		return ( a[ 0 ] * b[ 0 ] + a[ 1 ] * b[ 1 ] + a[ 2 ] * b[ 2 ] ) / ( aLen * bLen);
	}

	inline
	void cross( float target[], const float p1[], const float p2[] ) {
		target[0] = (p1[1] * p2[2]) - (p1[2] * p2[1]);
		target[1] = (p1[2] * p2[0]) - (p1[0] * p2[2]);
		target[2] = (p1[0] * p2[1]) - (p1[1] * p2[0]);
	}

	inline
	void scale( float target[], const float sf ) {
		target[ 0 ] *= sf;
		target[ 1 ] *= sf;
		target[ 2 ] *= sf;
	}

	inline
	void scale( float target[], const float source[ 3 ], const float sf ) {
		target[ 0 ] = source[ 0 ] * sf;
		target[ 1 ] = source[ 1 ] * sf;
		target[ 2 ] = source[ 2 ] * sf;
	}

	inline
	float length( const float source[] ) {
		return (float)sqrt( source[ 0 ] * source[ 0 ] + source[ 1 ] * source[ 1 ] + source[ 2 ] * source[ 2 ] );
	}

	inline
	void triangleNormal( const float tri[][ 3 ], float normal[ 3 ] ) {
		float v1[ 3 ], v2[ 3 ];
		sub3f( v1, tri[ 1 ], tri[ 0 ] );
		sub3f( v2, tri[ 2 ], tri[ 0 ] );
		cross( normal, v1, v2 );
	}

	inline
	float triangleArea( const float tri[][ 3 ] ) {
		float normal[ 3 ];
		triangleNormal( tri, normal );
		return length( normal ) / 2.0f;
	}

	inline
	void generatePointOnTriangle( const float tri[][ 3 ], float point[ 3 ] ) {
		float v1[ 3 ], v2[ 3 ];
		sub3f( v1, tri[ 1 ], tri[ 0 ] );
		sub3f( v2, tri[ 2 ], tri[ 0 ] );
		float u = RAND01, v = RAND01;
		if ( u + v > 1 ) {
			u = 1.0f - v;
			v = 1.0f - u;
		}		
		scale( v1, u );
		scale( v2, v );
		add3f( point, tri[ 0 ], v1 );
		add3f( point, v2 );
	}

	inline
	float distance( const float a[ 3 ], const float b[ 3 ] ) { 
		float vec[ 3 ];
		sub3f( vec, a, b );
		return sqrt( dot( vec, vec ) );
	}

	inline
	float maximum3( const float a, const float b, const float c ) {
		if ( a > b )
			return a > c ? a : c;
		else
			return b > c ? b : c;
	}

	inline
	void reflect( const float l[ 3 ], const float n[ 3 ], float *r ) { // l = light, n = normal, r = reflection vector
		float nl = -2*dot( n, l );
		r[0] = nl*n[0] + l[0];
		r[1] = nl*n[1] + l[1];
		r[2] = nl*n[2] + l[2];
	}

	inline
	void refract( const float l[ 3 ], const float n[ 3 ], const float sourceIOR, const float targetIOR, float *t ) { // l = light, n = normal, sourceIOR = source medium index of refraction, targetIOR = target medium index of refraction, r = reflection vector		
    float cos_i = dot( n, l ); 		    
		float b;
    if ( cos_i >= 0.0 ) 
			b = sourceIOR / targetIOR;
		else 
		  b = targetIOR / sourceIOR;
    float cos_r = 1.0f - ( b * b ) *( 1.0f - cos_i * cos_i );

    if ( cos_r >= 0.0f) {
			float a;
      if ( cos_i >= 0.0f)
        a = b * cos_i - sqrtf( cos_r ); 
      else
        a = b * cos_i + sqrtf( cos_r );
      t[ 0 ] = a * n[ 0 ] - b * l[ 0 ];
      t[ 1 ] = a * n[ 1 ] - b * l[ 1 ];
      t[ 2 ] = a * n[ 2 ] - b * l[ 2 ];
    } else {
			scale( t, n, cos_i * 2 );
			sub3f( t, t, l );      
    }
	}
};
#endif // _VECMATH_H_
