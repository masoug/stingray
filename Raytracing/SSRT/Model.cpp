/********************************************************************************************
 ** SSRT - Shaun's Simple Ray Tracer                                                       **
 **        By Shaun Nirenstein: shaun@cs.uct.ac.za                                         **
 **                             http://people.cs.uct.ac.za/~snirenst                       **
 **                                                                                        **
 ** License agreement:                                                                     **
 ** - This source code (or compiled version thereof) CANNOT be used freely for commercial  **
 **   purposes.  If you wish to use it for such purposes, please contact the author,       **
 **   Shaun Nirenstein (shaun@cs.uct.ac.za).                                               **
 ** - For educational purposes (personal or institutional), this source code may be        **
 **   modified, distributed or extended freely, as long as this license agreement appears  **
 **   at the top of every file as is.                                                      **
 ** - Any program modified or extended from a version of SSRT (or a modified or extended   **
 **   version thereof) must acknowledge that it is SSRT derived in an accessible about box **
 **   or credit screen.                                                                    **
 **                                                                                        **
 ********************************************************************************************
 */
#include <stdio.h>
#include <float.h>

// Stl
#include <list>
#include <string>
#include <fstream>
#include <algorithm>

// Custom
#include "VecMath.h"
#include "Model.h"
#include "RayBox.h"

using namespace std;

namespace Tracer {

Object::Object() {
	m_colour[ 0 ] = 1;
	m_colour[ 1 ] = 1;
	m_colour[ 2 ] = 1;
	m_vertices = NULL;
	m_indices = NULL;
	m_normals = NULL;
	m_vertexNormals = NULL;
	m_smooth = false;
	m_name = NULL;
	m_numIndices = 0;
	m_numVertices = 0;	
	m_lightSource = false;
	m_translation[ 0 ] = 0;
	m_translation[ 1 ] = 0;
	m_translation[ 2 ] = 0;
	m_diffuse = 1;
	m_specular = 0;
	m_transmission = 0;
	m_indexOfRefraction = 1.1f; ///< If it's refractive, it's usually going to be more dense than a vacuum ;-)
	m_specularIndex = 3;
	m_perfectSpecular = true;
}

Object::~Object() {
	if ( m_vertexNormals )
		delete[] m_vertexNormals;
	if ( m_vertices )
		delete[] m_vertices;
	if ( m_normals )
		delete[] m_normals;
	if ( m_indices )
		delete[] m_indices;
	if ( m_name )
		delete[] m_name;
}

Model::Model() {
	m_eye[ 0 ] = 0;
	m_eye[ 1 ] = 0;
	m_eye[ 2 ] = 0;
	m_direction[ 0 ] = 1;
	m_direction[ 1 ] = 0;
	m_direction[ 2 ] = 0;
	m_up[ 0 ] = 0;
	m_up[ 1 ] = 1;
	m_up[ 2 ] = 0;
	m_isLoaded = false;
	m_epsilon = 0.0001f;
	m_lightSamples = 5;	
	m_lightSampleRatio = 1.0f;
	m_saturation = 0;
	m_pathDepth = 1;	
}

Model::~Model() {
	clear();
}

int Model::load( const char *fileName ) {
	ifstream in( fileName );
	if ( !in.is_open() )
		return 1;	
		
  list<Object*> scene;
	Object *currentObject = NULL;

	// Check file format
	string str, att, eq, val;
	in >> str >> eq >> val;
	if ( str != "SSRT" && eq != "Input" && val != "File" )
		return 2;

	m_numObjects = 0;
	int numDefObjects = 0;
	bool target = false;
	while( !in.eof() ) {
		in >> att;		
		if ( in.eof() )
			break; // white spaces threw off the while's feof
		if ( att == "#" ) { // Handle comments
			in >> att;
			while( att != "#" )
				in >> att;
			continue;
		}
		in >> eq;
		if ( eq != "=" )
			return 2;
		transform( att.begin(), att.end(), att.begin(), (int (*)(int))tolower );		

		// Handle attribute values
		if ( att == "version" )                                      // VERSION
			in >> m_version;
		else if ( att == "eye" ) {                                   // EYE
			in >> m_eye[ 0 ] >> m_eye[ 1 ] >> m_eye[ 2 ];
		} else if ( att == "direction" ) {                           // DIRECTION
			in >> m_direction[ 0 ] >> m_direction[ 1 ] >> m_direction[ 2 ];			
		} else if ( att == "target" ) {                              // TARGET ( direction by target)
			in >> m_direction[ 0 ] >> m_direction[ 1 ] >> m_direction[ 2 ];						
			target = true;
		} else if ( att == "up" ) {                                  // UP
			in >> m_up[ 0 ] >> m_up[ 1 ] >> m_up[ 2 ];									
		} else if ( att == "width" ) {                               // WIDTH
			in >> m_width;
		} else if ( att == "height" ) {                              // HEIGHT
			in >> m_height;
		} else if ( att == "lightsamples" ) {                        // LIGHTSAMPLES
			in >> m_lightSamples;	
		} else if ( att == "lightsampleratio" ) {                    // LIGHTSAMPLERATIO
			in >> m_lightSampleRatio;
		} else if ( att == "raysperpixel" ) {                        // RAYSPERPIXEL
			in >> m_raysPerPixel;
		} else if ( att == "pathdepth" ) {                           // RAYSPERPIXEL
			in >> m_pathDepth ;
		} else if ( att == "saturation" ) {                          // SATURATION
			in >> m_saturation;
		}	else if ( att == "object" ) {                              // OBJECT
			in >> val;
			transform( val.begin(), val.end(), val.begin(), (int (*)(int))tolower );
			currentObject = new Object();
			scene.push_back( currentObject );
			currentObject->m_name = new char[ val.length() + 1 ];
			strcpy( currentObject->m_name, val.c_str() );
			m_numObjects++;
		} else if ( att == "diffuse" ) {                             // DIFFUSE
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_diffuse;			
		} else if ( att == "specular" ) {                            // SPECULAR
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_specular;
		} else if ( att == "transmission" ) {                        // TRANSMISSION
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_transmission;
		} else if ( att == "indexofrefraction" ) {                   // INDEXOFREFRACTION
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_indexOfRefraction;
		} else if ( att == "specularindex" ) {                       // SPECULARINDEX
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_specularIndex;
		} else if ( att == "perfectspecular" ) {                     // PERFECTSPECULAR
			if ( !currentObject ) 
				return 3;
			in >> val;
			transform( val.begin(), val.end(), val.begin(), (int (*)(int))tolower );
			if ( val == "false" )
				currentObject->m_perfectSpecular = false;
			else
				currentObject->m_lightSource = false;
		} else if ( att == "translation" ) {                         // TRANSLATION
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_translation[ 0 ] >> currentObject->m_translation[ 1 ] >> currentObject->m_translation[ 2 ];			
		}
		else if ( att == "colour" || att == "color" ) {              // COLOUR
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_colour[ 0 ] >> currentObject->m_colour[ 1 ] >> currentObject->m_colour[ 2 ];			
		} else if ( att == "lightsource" ) {                         // LIGHTSOURCE
			if ( !currentObject ) 
				return 3;
			in >> val;
			transform( val.begin(), val.end(), val.begin(), (int (*)(int))tolower );
			if ( val == "true" )
				currentObject->m_lightSource = true;
			else
				currentObject->m_lightSource = false;
	  } else if ( att == "smooth" ) {                              // SMOOTH
			if ( !currentObject ) 
				return 3;
			in >> val;
			transform( val.begin(), val.end(), val.begin(), (int (*)(int))tolower );
			if ( val == "true" )
				currentObject->m_smooth = true;
			else
				currentObject->m_smooth = false;			
		} else if ( att == "faces" ) {                               // FACES
			if ( !currentObject ) 
				return 3;
			int faces;
			in >> faces;
			currentObject->m_numIndices = faces * 3;
		} else if ( att == "vertices" ) {                            // VERTICES
			if ( !currentObject ) 
				return 3;
			in >> currentObject->m_numVertices;
		} else if ( att == "objectdef" ) {                           // OBJECTDEF
			// Search for name
			in >> val;
			transform( val.begin(), val.end(), val.begin(), (int (*)(int))tolower );
			list<Object*>::iterator it;
			Object *obj = NULL;
			// Find object
			for ( it = scene.begin( ); it != scene.end( ); it++ )
				if ( (*it)->m_name == val )
					obj = *it;				
			if ( !obj )
				return 4;		
			
			// Load vertices
			obj->m_vertices = new float[ obj->m_numVertices * 3 ];
			for( int i = 0; i < obj->m_numVertices * 3; i++ ) {
				in >> obj->m_vertices[ i ];
				obj->m_vertices[ i ] += obj->m_translation[ i % 3 ];
			}

			// Load indices
			obj->m_indices = new int[ obj->m_numIndices ];
			for( int i = 0; i < obj->m_numIndices; i++ )
				in >> obj->m_indices[ i ];
			numDefObjects++;
		} else {
			return 5;
		}
	}	
	in.close();

	if ( numDefObjects != m_numObjects )
		return 6; // Number of object declarations don't match the number of objects with data

	// Fix up direction to point to target
	if ( target ) {
		m_direction[ 0 ] -= m_eye[ 0 ];
		m_direction[ 1 ] -= m_eye[ 1 ];
		m_direction[ 2 ] -= m_eye[ 2 ];
	}

	// Now convert list into an array
	m_objects = new Object[ m_numObjects ];
	list<Object*>::iterator it;
	int pos = 0;
	for ( it = scene.begin( ); it != scene.end( ); it++, pos++ ) {
		m_objects[ pos ] = **it; // Shallow copy
		delete (void*)(*it); // Erase old copy - Shallow erase: Casting to void won't call the ~Object destructor
	}	

	// Generate auxiliary information
	generateNormals();
	generateBounds();
	m_isLoaded = true;
	return 0;
}

/**
 * Generate normals for each object.  Triangle normals are computed for ALL objects.
 * Vertex normals are computed for those objects that are "smooth".
 */
void Model::generateNormals() {
	for( int objectIndex = 0; objectIndex < m_numObjects; objectIndex++ ) {
		Object *object = &m_objects[ objectIndex ];
		float tri[ 3 ][ 3 ];
		object->m_normals = new float[ object->m_numIndices ]; // One normal per face (3 components, per normal and 3 indices per face)
		object->m_planeConstants = new float[ object->m_numIndices / 3 ]; // One constant per face			
		for( int i = 0; i < object->m_numIndices / 3; i++ ) {
			getTriangle( objectIndex, i, tri );
			Utils::triangleNormal( tri, &object->m_normals[ i * 3 ] );
			Utils::normalise( &object->m_normals[ i * 3 ] );
			object->m_planeConstants[ i ] = Utils::dot( &object->m_normals[ i * 3 ], tri[ 0 ] ); // Compute plane constant of triangle
		}
		if ( object->m_smooth ) {
			object->m_vertexNormals = new float[ object->m_numVertices * 3 ]; // One normal per vertex
			for( int i = 0; i < object->m_numVertices * 3; i++ )
				object->m_vertexNormals[ i ] = 0.0f; // Zero normals
			for( int i = 0; i < object->m_numIndices / 3; i++ ) { // Add up normal				
				Utils::add3f( &object->m_vertexNormals[ object->m_indices[ i * 3     ] * 3 ], &object->m_normals[ i * 3 ] ); // Add face normals to vertices
				Utils::add3f( &object->m_vertexNormals[ object->m_indices[ i * 3 + 1 ] * 3 ], &object->m_normals[ i * 3 ] );
				Utils::add3f( &object->m_vertexNormals[ object->m_indices[ i * 3 + 2 ] * 3 ], &object->m_normals[ i * 3 ] ); 
			}
			for( int i = 0; i < object->m_numVertices; i++ )
				Utils::normalise( &object->m_vertexNormals[ i * 3 ] );
		}
	}
}

/**
 * Intersect a ray with the model.  The index of the object that is hit is returned (since this is
 * required to index into the relevant material data).  The intersection point is returned (since
 * the reflected/refracted ray will require this as an origin).  The normal is also returned (for computing  
 * local illumination, and for generating the next ray in the path in accordance to the correct distribution).
 * The normal is interpolated using the barycentric coordinates of the intersection, if the hit object is smooth.
 * True is returned iff the ray does intersect an object.
 */
bool Model::rayIntersect( const float origin[ 3 ], const float dir[ 3 ], int &hitObjectIndex, float intersectionPoint[ 3 ], float normal[ 3 ] ) {
	float tMin = FLT_MAX, uMin, vMin;
	int nearestObject = -1, nearestTriangle = -1;
	for( int objectIndex = 0; objectIndex < m_numObjects; objectIndex++ ) {
		Object *object = &m_objects[ objectIndex ];
		float coord[ 3 ];
		if ( !RayAABB( object->m_bounds, origin, dir, coord ) ) // Trivial rejection of whole object (Bounding Box check)
			continue;
		float tri[ 3 ][ 3 ];
		for( int i = 0; i < object->m_numIndices / 3; i++ ) {
			getTriangle( objectIndex, i, tri );			
			float t, u, v;
			if ( rayTriangleTest( tri, origin, dir, t, u, v ) ) {
				if ( tMin > t ) {
					tMin = t;
					nearestObject = objectIndex;
					nearestTriangle = i;
					uMin = u;
					vMin = v;
				}
			}
		}
	}
	if ( nearestObject != -1 ) { // Is there a nearest object, or did the ray just shoot off into infinity?
		hitObjectIndex = nearestObject;
		intersectionPoint[ 0 ] = origin[ 0 ] + tMin * dir[ 0 ];
		intersectionPoint[ 1 ] = origin[ 1 ] + tMin * dir[ 1 ];
		intersectionPoint[ 2 ] = origin[ 2 ] + tMin * dir[ 2 ];
		if ( m_objects[ nearestObject ].m_smooth == false )
			Utils::setf3( normal, &m_objects[ nearestObject ].m_normals[ nearestTriangle * 3 ] ); // Triangle normal
		else {
			float normals[ 3 ][ 3 ];
			getNormals( nearestObject, nearestTriangle, normals );
			float a = 1 - uMin - vMin;
			float b = uMin;
			float c = vMin;
			normal[ 0 ] = a * normals[ 0 ][ 0 ] + b * normals[ 1 ][ 0 ] + c * normals[ 2 ][ 0 ];
			normal[ 1 ] = a * normals[ 0 ][ 1 ] + b * normals[ 1 ][ 1 ] + c * normals[ 2 ][ 1 ];
			normal[ 2 ] = a * normals[ 0 ][ 2 ] + b * normals[ 1 ][ 2 ] + c * normals[ 2 ][ 2 ];			
			Utils::normalise( normal ); // Interpolated normal
		}			
		return true;
	} else
		return false;
}

/**
 * Check a ray against a triangle.
 * Code by Thomas Akenine-Moller: http://www.realtimerendering.com
 */
bool Model::rayTriangleTest( const float tri[ 3 ][ 3 ], const float s[ 3 ], const float d[ 3 ], float &intersection, float &barU, float &barV ) {
	// Thomas Moller's triangle intersection code
	float edge1[ 3 ], edge2[ 3 ], tVec[ 3 ], pVec[ 3 ], qVec[ 3 ];
	float det, invDet, u, v, tVal;

	Utils::sub3f( edge1, tri[ 1 ], tri[ 0 ] );
	Utils::sub3f( edge2, tri[ 2 ], tri[ 0 ] );
	Utils::cross( pVec, d, edge2 );
	det = Utils::dot( edge1, pVec );
	Utils::sub3f( tVec, s, tri[ 0 ] );
	if ( det > 0.0f ) {
		u = Utils::dot( tVec, pVec );
		if ( u < 0.0f || u > det )
			return false;
		Utils::cross( qVec, tVec, edge1 );
		v = Utils::dot( d, qVec );
		if ( v < 0.0f || u + v > det )
			return false;
	} else
		return false;
	invDet = 1.0f / det;
	tVal = Utils::dot( edge2, qVec ) * invDet;
	if ( tVal >= m_epsilon ) {
		intersection = tVal;
		barU = u * invDet;
		barV = v * invDet;
		return true;
	}
	return false;
}

/**
 * Shadow ray test.  Returns true if shadowed, or false otherwise.
 * No other information is needed.
 */
bool Model::testShadowRay( const float origin[ 3 ], const float dir[ 3 ] ) {
	for( int objectIndex = 0; objectIndex < m_numObjects; objectIndex++ ) {
		Object *object = &m_objects[ objectIndex ];
		float coord[ 3 ];
		if ( !RayAABB( object->m_bounds, origin, dir, coord ) ) // Trivial rejection of whole object
			continue;
		float tri[ 3 ][ 3 ];
		for( int i = 0; i < object->m_numIndices / 3; i++ ) {
			getTriangle( objectIndex, i, tri );			
			float t, u, v;
			if ( rayTriangleTest( tri, origin, dir, t, u, v ) && t < 1 - m_epsilon ) // t < 1 will check the segment.
				return false;
		}
	}
	return true;
}

/**
 * Generate the bounding boxes for each object.
 */
void Model::generateBounds() {
	for( int objectIndex = 0; objectIndex < m_numObjects; objectIndex++ ) {		
		float *bounds = m_objects[ objectIndex ].m_bounds;
		float *vertices = m_objects[ objectIndex ].m_vertices;
		Utils::setf3( bounds, vertices );
		Utils::setf3( &bounds[ 3 ], vertices ); // Set bounds to be exactly the first vertex
		for( int i = 1; i < m_objects[ objectIndex ].m_numVertices; i++ ) {
			if ( bounds[ 0 ] > vertices[ i * 3 ] ) 
				bounds[ 0 ] = vertices[ i * 3 ];
			if ( bounds[ 1 ] > vertices[ i * 3 + 1 ] ) 
				bounds[ 1 ] = vertices[ i * 3 + 1 ];
			if ( bounds[ 2 ] > vertices[ i * 3 + 2 ] ) 
				bounds[ 2 ] = vertices[ i * 3 + 2 ];
			if ( bounds[ 3 ] < vertices[ i * 3 ] ) 
				bounds[ 3 ] = vertices[ i * 3 ];
			if ( bounds[ 4 ] < vertices[ i * 3 + 1 ] ) 
				bounds[ 4 ] = vertices[ i * 3 + 1 ];
			if ( bounds[ 5 ] < vertices[ i * 3 + 2 ] ) 
				bounds[ 5 ] = vertices[ i * 3 + 2 ];
		}
	}
}

/**
 * Get the triangle belonging to object objectIndex at index triangleIndex.
 */
void Model::getTriangle( const int objectIndex, const int triangleIndex, float tri[ 3 ][ 3 ] ) {
	Object *obj = &m_objects[ objectIndex ];
	Utils::setf3( tri[ 0 ], &obj->m_vertices[ obj->m_indices[ triangleIndex * 3     ] * 3 ] );
	Utils::setf3( tri[ 1 ], &obj->m_vertices[ obj->m_indices[ triangleIndex * 3 + 1 ] * 3 ] );
	Utils::setf3( tri[ 2 ], &obj->m_vertices[ obj->m_indices[ triangleIndex * 3 + 2 ] * 3 ] );
}

/**
 * Get the triangle normal belonging to the triangle in object objectIndex at index triangleIndex.
 */
void Model::getNormals( const int objectIndex, const int triangleIndex, float normals[ 3 ][ 3 ] ) {
	Object *obj = &m_objects[ objectIndex ];
	Utils::setf3( normals[ 0 ], &obj->m_vertexNormals[ obj->m_indices[ triangleIndex * 3     ] * 3 ] );
	Utils::setf3( normals[ 1 ], &obj->m_vertexNormals[ obj->m_indices[ triangleIndex * 3 + 1 ] * 3 ] );
	Utils::setf3( normals[ 2 ], &obj->m_vertexNormals[ obj->m_indices[ triangleIndex * 3 + 2 ] * 3 ] );
}

/**
 * Free the memory for all objects.  There is no othe heap data.
 */
void Model::clear() {
	delete[] m_objects; // The objects will delete their own data.
	m_objects = NULL;
}

};
