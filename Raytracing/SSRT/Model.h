/********************************************************************************************
 ** SSRT - Shaun's Simple Ray Tracer                                                       **
 **        By Shaun Nirenstein: shaun@cs.uct.ac.za                                         **
 **                             http://people.cs.uct.ac.za/~snirenst                       **
 **                                                                                        **
 ** License agreement:                                                                     **
 ** - This source code (or compiled version thereof) CANNOT be used freely for commercial  **
 **   purposes.  If you wish to use it for such purposes, please contact the author,       **
 **   Shaun Nirenstein (shaun@cs.uct.ac.za).                                               **
 ** - For educational purposes (personal or institutional), this source code may be        **
 **   modified, distributed or extended freely, as long as this license agreement appears  **
 **   at the top of every file as is.                                                      **
 ** - Any program modified or extended from a version of SSRT (or a modified or extended   **
 **   version thereof) must acknowledge that it is SSRT derived in an accessible about box **
 **   or credit screen.                                                                    **
 **                                                                                        **
 ********************************************************************************************
 */
#ifndef _MODEL_H_
#define _MODEL_H_

namespace Tracer {

class Object {
 public:
	Object();
	~Object();

	// Special
	char *m_name;	

  // Material data
	float m_diffuse;
	float m_specular;
	float m_transmission;
	float m_specularIndex;
  float m_colour[ 3 ];	
	bool m_smooth;
	bool m_perfectSpecular;
	float m_indexOfRefraction;
  
	// Geometry data
	float m_bounds[ 6 ];
	int m_numVertices;
	float *m_vertices;
	int m_numIndices; ///< Number of triangles * 3
	int *m_indices;	
	bool m_lightSource;
	float *m_normals;
	float *m_planeConstants; ///< Such that normal i ( &m_normals[ 3 * i ] ) is the coefficents of the plane normal.X = D, where D is m_planeCosntants[ i ]
	float *m_vertexNormals;
	float m_translation[ 3 ];	
};

class Model {
 public:
	Model();
	~Model();
	int load( const char *fileName );
	bool isLoaded() { return m_isLoaded; }
	void clear(); 

	// Geometry
	bool rayIntersect( const float origin[ 3 ], const float dir[ 3 ], int &hitObjectIndex, float intersectionPoint[ 3 ], float normal[ 3 ] );
	bool rayTriangleTest( const float tri[ 3 ][ 3 ], const float s[ 3 ], const float d[ 3 ], float &intersection, float &barU, float &barV );	
	void getTriangle( const int objectIndex, const int triangleIndex, float tri[ 3 ][ 3 ] );
	void getNormals( const int objectIndex, const int triangleIndex, float normals[ 3 ][ 3 ] );
	bool testShadowRay( const float origin[ 3 ], const float dir[ 3 ] );
	float m_epsilon;
	int m_version;
	int m_numObjects;
	Object *m_objects;

	// Camera
	float m_eye[ 3 ], m_direction[ 3 ], m_up[ 3 ];	

	// Image
	int m_width, m_height;
	float m_saturation;

	// Light control
	int m_lightSamples;
	float m_lightSampleRatio;

	// Sample control
	int m_raysPerPixel;
	int m_pathDepth;

 protected:
	void generateNormals();
	void generateBounds();
	bool m_isLoaded;
};

};

#endif // _MODEL_H_