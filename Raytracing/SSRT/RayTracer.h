/********************************************************************************************
 ** SSRT - Shaun's Simple Ray Tracer                                                       **
 **        By Shaun Nirenstein: shaun@cs.uct.ac.za                                         **
 **                             http://people.cs.uct.ac.za/~snirenst                       **
 **                                                                                        **
 ** License agreement:                                                                     **
 ** - This source code (or compiled version thereof) CANNOT be used freely for commercial  **
 **   purposes.  If you wish to use it for such purposes, please contact the author,       **
 **   Shaun Nirenstein (shaun@cs.uct.ac.za).                                               **
 ** - For educational purposes (personal or institutional), this source code may be        **
 **   modified, distributed or extended freely, as long as this license agreement appears  **
 **   at the top of every file as is.                                                      **
 ** - Any program modified or extended from a version of SSRT (or a modified or extended   **
 **   version thereof) must acknowledge that it is SSRT derived in an accessible about box **
 **   or credit screen.                                                                    **
 **                                                                                        **
 ********************************************************************************************
 */
#ifndef _RAYTRACER_H_
#define _RAYTRACER_H_

namespace Tracer {

class PointLight {
 public:
	PointLight():m_objectIndex( -1 ), m_triangleIndex( -1 ) {}
  float m_point[ 3 ];
	float m_plane[ 4 ]; ///< Plane in which the light source lies (i.e., the plane of the area lightsource from which it was computed)
	int m_objectIndex; ///< Pointer back to object containing light source
	int m_triangleIndex; ///< Pointer back to triangle containing light source
};

class RayTracer {
 public:
	RayTracer( Model *model, unsigned char *bitmap );
	~RayTracer();
	bool isStillTracing() { return m_tracing; }
	void traceLine();
	void updateBitmap();
	float getProgress();

 protected:
	void tracePixel( const float u, const float v, float pixel[ 3 ] );
	void traceRay( const float origin[ 3 ], const float dir[ 3 ], float pixel[ 3 ] );
	void computeDirectContribution( const float intersectionPoint[ 3 ], const float normal[ 3 ], const int hitObjectIndex, float direct[ 3 ] );	

	Model *m_model;
	bool m_tracing;
	unsigned char *m_bitmap;
	int m_currentLine;
	float *m_pixels;
	float m_pixelSize[ 2 ];

	// Camera
	float m_uVec[ 3 ], m_vVec[ 3 ], m_corner[ 3 ];

	// Light sources
	void generateLightSourcePoints();
	PointLight *m_lights; ///< Point light sources (note: these are constructed from emitter polygons to approximate area light sources)	
	int m_numLights; ///< Number of PointLights in m_lights

	// Sample tables
	void generateDiffuseRayTable();
	void generateRandomSequences();
	void getDiffuseRay( const float normal[ 3 ], const int depth, float ray[ 3 ] );
	void getSpecularRay( const float reflection[ 3 ], const float specularIndex, float ray[ 3 ] );
	int m_samplesPerStrata;
	float *m_diffuseTable;
	int m_currentSample; ///< m_currentSample is the current sample position (i.e., which strata or ray)
	int m_numberOfRandomSequences;
	int **m_randomSequences;
	int *m_strataSequences;
};


};
#endif // _RAYTRACER_H_