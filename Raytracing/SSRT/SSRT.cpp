/********************************************************************************************
 ** SSRT - Shaun's Simple Ray Tracer                                                       **
 **        By Shaun Nirenstein: shaun@cs.uct.ac.za                                         **
 **                             http://people.cs.uct.ac.za/~snirenst                       **
 **                                                                                        **
 ** License agreement:                                                                     **
 ** - This source code (or compiled version thereof) CANNOT be used freely for commercial  **
 **   purposes.  If you wish to use it for such purposes, please contact the author,       **
 **   Shaun Nirenstein (shaun@cs.uct.ac.za).                                               **
 ** - For educational purposes (personal or institutional), this source code may be        **
 **   modified, distributed or extended freely, as long as this license agreement appears  **
 **   at the top of every file as is.                                                      **
 ** - Any program modified or extended from a version of SSRT (or a modified or extended   **
 **   version thereof) must acknowledge that it is SSRT derived in an accessible about box **
 **   or credit screen.                                                                    **
 **                                                                                        **
 ********************************************************************************************
 */

// SSRT.cpp : Defines the entry point for the application.
//

#define WIN32_LEAN_AND_MEAN	
#include <windows.h>

#include <Commdlg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <tchar.h>
#define MAX_LOADSTRING 100

// Custom
#include "SSRT.h"
#include "Model.h"
#include "RayTracer.h"

// SSRT Additions
HBITMAP g_renderBitmap = NULL;
unsigned char *g_data = NULL;
HDC g_src = NULL;
float g_renderTime = 0;
float g_progress = 0;

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HACCEL hAccelTable;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	MSG msg;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SSRT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_SSRT);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_SSRT);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCTSTR)IDC_SSRT;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	 

   if (!hWnd)
   {
      return FALSE;
   }
   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message) 
	{
	case WM_CLOSE:
		exit( 0 );
		break;
	case WM_QUIT:
		exit( 0 );
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_FILE_LOADFILE:
			OPENFILENAME ofn;
			ZeroMemory( &ofn, sizeof( OPENFILENAME ) );
			ofn.lpstrFilter = "Scene\0*.ssrt\0";
			ofn.nFilterIndex = 1;						
			ofn.hwndOwner = hWnd;
			char file[ 2048 ];
			ZeroMemory( file, 2048 );
			ofn.nMaxFile = 2048;
			ofn.lpstrFile = file;
			ofn.lStructSize = sizeof( OPENFILENAME );
			if ( GetOpenFileName( &ofn ) ) {
				Tracer::Model model;
				int ret = model.load( ofn.lpstrFile );
				if ( ret ) {
					char str[ 1024 ];
					sprintf( str, "Could not load file %s\nError code: %d", ofn.lpstrFile, ret );
					MessageBox( hWnd, str, "Error", MB_OK );
					return 0;
				}
				RECT rect;
				GetClientRect( hWnd, &rect ); 				
				g_renderBitmap = CreateCompatibleBitmap( GetDC( hWnd ), model.m_width, model.m_height );
				g_src = CreateCompatibleDC( GetDC( hWnd ) );
				if ( g_data )
					delete[] g_data;
				g_data = new unsigned char[ model.m_width * model.m_height * 4 ];												

				// Start ray tracer
				Tracer::RayTracer tracer( &model, g_data );
				int startTime = clock();
				g_progress = 0;
				while( tracer.isStillTracing() ) {
					// Redraw				
					InvalidateRect( hWnd, &rect, false );
					UpdateWindow( hWnd );		

					// Trace a line
					tracer.traceLine();
					tracer.updateBitmap();

					// Update timer / progress
					g_renderTime = (float)( clock() - startTime ) / (float)CLOCKS_PER_SEC;
					g_progress = tracer.getProgress();

					// Handle messages
					MSG msg;					
					while( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
						if ( !TranslateAccelerator( msg.hwnd, hAccelTable, &msg ) )  {
							TranslateMessage(&msg);
							DispatchMessage(&msg);
						}
				}
				// Redraw				
				InvalidateRect( hWnd, &rect, false );
				UpdateWindow( hWnd );
			}			
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		{ 
		hdc = BeginPaint(hWnd, &ps);
		
		// Clear window
		RECT rect;
		GetClientRect( hWnd, &rect );
		HBRUSH brush = CreateSolidBrush( RGB( 0, 0, 0 ) );
		FillRect( hdc, &rect, brush );
		
		if ( g_renderBitmap ) {
			// Draw bitmap			
			BITMAP bmInfo;
			GetObject( g_renderBitmap, sizeof( BITMAP ), &bmInfo );			
			SetBitmapBits( g_renderBitmap, bmInfo.bmWidth * bmInfo.bmHeight * 4, g_data );
			int middleH = ( rect.left + rect.right ) / 2;
			int middleV = ( rect.bottom + rect.top ) / 2;			
			HGDIOBJ temp = SelectObject( g_src, g_renderBitmap );
			BitBlt( hdc, middleH - bmInfo.bmWidth / 2, middleV - bmInfo.bmHeight / 2, bmInfo.bmWidth, bmInfo.bmHeight, g_src, 0, 0, SRCCOPY );
			SelectObject( g_src, temp );
		}
		char timer[ 256 ];
		sprintf( timer, "Time elapsed: %f", g_renderTime );
		TextOut( hdc, 0, 0, timer, (int)strlen( timer ) );
		if ( g_progress > 0 ) {
			sprintf( timer, "Eta: %f", g_renderTime / g_progress - g_renderTime );
			TextOut( hdc, 0, 32, timer, (int)strlen( timer ) );
		}
		EndPaint(hWnd, &ps);
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
	return FALSE;
}
