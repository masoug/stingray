/*
    This file belongs to the Ray tracing tutorial of http://www.codermind.com/
    It is free to use for educational purpose and cannot be redistributed
    outside of the tutorial pages.
    Any further inquiry :
    mailto:info@codermind.com
 */

#ifndef __RAYTRACE_H
#define __RAYTRACE_H

#include "Def.h"

struct material {
    float reflection;
	float red, green, blue;
};

struct sphere {
	point pos;
	float size;
	int materialId;
};

struct light {
	point pos;
	float red, green, blue;
};

struct ray {
	point start;
	vecteur dir;
};

#endif // __RAYTRACE_H
