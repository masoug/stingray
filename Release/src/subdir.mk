################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Light.cpp \
../src/Material.cpp \
../src/Object.cpp \
../src/Ray.cpp \
../src/Scene.cpp \
../src/Shader.cpp \
../src/Spectrum.cpp \
../src/Sphere.cpp \
../src/Vector.cpp \
../src/main.cpp \
../src/tinystr.cpp \
../src/tinyxml.cpp \
../src/tinyxmlerror.cpp \
../src/tinyxmlparser.cpp 

OBJS += \
./src/Light.o \
./src/Material.o \
./src/Object.o \
./src/Ray.o \
./src/Scene.o \
./src/Shader.o \
./src/Spectrum.o \
./src/Sphere.o \
./src/Vector.o \
./src/main.o \
./src/tinystr.o \
./src/tinyxml.o \
./src/tinyxmlerror.o \
./src/tinyxmlparser.o 

CPP_DEPS += \
./src/Light.d \
./src/Material.d \
./src/Object.d \
./src/Ray.d \
./src/Scene.d \
./src/Shader.d \
./src/Spectrum.d \
./src/Sphere.d \
./src/Vector.d \
./src/main.d \
./src/tinystr.d \
./src/tinyxml.d \
./src/tinyxmlerror.d \
./src/tinyxmlparser.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


