/*
 * Sphere.h
 *
 *  Created on: Nov 8, 2011
 *      Author: Sammy
 */

#ifndef SPHERE_H_
#define SPHERE_H_

#include "stdlib.h"
#include "Ray.h"
#include "Object.h"
#include "Material.h"

class Sphere : public Object
{
public:
	Sphere(double rad, const Vector &pos, const Material &mat);
	bool Intersect(const Ray &ray, double &distance);
	Vector GetNormal(const Vector &pos) const;

	double radius;
};

#endif /* SPHERE_H_ */
