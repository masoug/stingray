/*
 * Sphere.cpp
 *
 *  Created on: Nov 8, 2011
 *      Author: Sammy
 */

#include "Sphere.h"
#include <iostream>

Sphere::Sphere(double rad, const Vector &pos, const Material &mat) : Object(), radius(rad) {
	position = pos;
	name = "Sphere";
	material = mat;
}

bool Sphere::Intersect(const Ray &ray, double &distance) // Return distance to sphere.
{
	double b = (position - ray.origin).Dot(ray.direction);
	double discr = (b * b) - (position - ray.origin).Dot(position - ray.origin)
			+ (radius * radius);
	if (discr < 0)
		return false;
	else if (discr > 0 || discr == 0) {
		double sol1 = b - sqrt(discr);
		double sol2 = b + sqrt(discr);
		if (sol1 > sol2 && sol2 < distance) {
			distance = sol2;
			return true;
		} else if (sol1 < sol2 && sol1 < distance) {
			distance = sol1;
			return true;
		} else if (distance <= 0)
			return false;
	}
}

Vector Sphere::GetNormal(const Vector &pos) const {
	return (pos - position) * (1.0 / radius);
}
