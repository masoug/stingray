/*
 * Vector.h
 *
 * 3D vector implementation.
 *
 *  Created on: Nov 8, 2011
 *      Author: Sammy
 */

#include "math.h"

#ifndef VECTOR_H_
#define VECTOR_H_

class Vector {
public:
	Vector(double x_ = 0, double y_ = 0, double z_ = 0);
	Vector operator+(const Vector &b) const {
		return Vector(x + b.x, y + b.y, z + b.z);
	}
	Vector operator-(const Vector &b) const {
		return Vector(x - b.x, y - b.y, z - b.z);
	}
	Vector operator*(double b) const {
		return Vector(x * b, y * b, z * b);
	}
	Vector operator*(const Vector &b) const {
		return Vector(x * b.x, y * b.y, z * b.z);
	}
	Vector operator%(Vector &b) {
		return Vector(y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x);
	}
	Vector& Normalize();
	double Dot(const Vector &b) const;
	double Length();

	double x, y, z;
};

#endif /* VECTOR_H_ */
