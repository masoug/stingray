/*
 * Shader.h
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#ifndef SHADER_H_
#define SHADER_H_

#include "Vector.h"
#include "Light.h"
#include "Utils.h"
#include "Spectrum.h"
#include "Material.h"
#include "stdlib.h"

double DistanceSqr(const Vector &start, const Vector &end);

void Lambert(const Material &mat, Vector &light, Vector &normal, Spectrum &spec);

void LightFalloff(Light &light, Vector &poi, Spectrum &spec);

#endif /* SHADER_H_ */
