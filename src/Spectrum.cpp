/*
 * Spectrum.cpp
 *
 *  Created on: Nov 30, 2011
 *      Author: Sammy
 */

#include "Spectrum.h"

Spectrum::Spectrum(double arr, double gee, double bee) :
	r(arr), g(gee), b(bee)
{
}

void Spectrum::operator+=(const Spectrum &spec)
{
	r += spec.r;
	g += spec.g;
	b += spec.b;
}

Spectrum& Spectrum::operator=(const Spectrum &spec)
{
	if (this == &spec)
		return *this;
	r = spec.r;
	g = spec.g;
	b = spec.b;
	return *this;
}

void Spectrum::operator*=(const Spectrum &spec)
{
	r *= spec.r;
	g *= spec.g;
	b *= spec.b;
}

void Spectrum::operator*=(const double constant)
{
	r *= constant;
	g *= constant;
	b *= constant;
}

Spectrum Spectrum::operator*(const Spectrum &spec) const
{
	return Spectrum(r * spec.r, g * spec.g, b * spec.b);
}

Spectrum Spectrum::operator*(const double constant) const
{
	return Spectrum(r * constant, g * constant, b * constant);
}

Spectrum Spectrum::operator/(const Spectrum &spec) const
{
	return Spectrum(r / spec.r, g / spec.g, b / spec.b);
}

Spectrum Spectrum::operator+(const Spectrum &spec) const
{
	return Spectrum(r + spec.r, g + spec.g, b + spec.b);
}

Spectrum Spectrum::operator+(const double constant) const
{
	return Spectrum(r + constant, g + constant, b + constant);
}

Spectrum Spectrum::operator-(const Spectrum &spec) const
{
	return Spectrum(r - spec.r, g - spec.g, b - spec.b);
}

Spectrum::~Spectrum() {
}
