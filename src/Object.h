/*
 * Object.h
 *
 * Abstract base class for all renderable objects.
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#ifndef OBJECT_H_
#define OBJECT_H_

#include "Vector.h"
#include "Ray.h"
#include "Material.h"
#include "stdlib.h"
#include <iostream>
#include <string>

class Object {
public:
	Object();
	virtual bool Intersect(const Ray &ray, double &distance) = 0;
	virtual Vector GetNormal(const Vector &pos) const = 0;
	Material GetMaterial() {return material;}
	Vector GetPosition() {return position;}
	std::string GetName() {return name;}

	Material material;
	Vector position;
	std::string name;
};

#endif /* OBJECT_H_ */
