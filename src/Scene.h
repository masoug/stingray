/*
 * Scene.h
 *
 * Basic scene implementation.
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#ifndef SCENE_H_
#define SCENE_H_

#include "tinyxml.h"
#include "Object.h"
#include "Light.h"
#include "Spectrum.h"
#include "stdlib.h"
#include <vector>

class Scene
{
public:
	Scene(const char* filename);
	bool Initialize();

	std::string name;
	std::vector<Object*> objects;
	std::vector<Light> lights;

private:
	TiXmlDocument file;
};

#endif /* SCENE_H_ */
