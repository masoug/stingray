/*
 * Vector.cpp
 *
 *  Created on: Nov 8, 2011
 *      Author: Sammy
 */

#include "Vector.h"

Vector::Vector(double x_, double y_, double z_) {
	x = x_;
	y = y_;
	z = z_;
}

Vector& Vector::Normalize() {
	return *this = *this * (1 / sqrt(x * x + y * y + z * z));
}

double Vector::Dot(const Vector &b) const {
	return x * b.x + y * b.y + z * b.z;
}

double Vector::Length() {
	return sqrt(x * x + y * y + z * z);
}
