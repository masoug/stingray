/*
 * Ray.cpp
 *
 *  Created on: Nov 8, 2011
 *      Author: Sammy
 */

#include "Ray.h"

Ray::Ray(const Vector &orig, const Vector &dir) :
	origin(orig.x, orig.y, orig.z), direction(dir.x, dir.y, dir.z) {
}
