/*
 * Scene.cpp
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#include "Scene.h"
#include "Sphere.h"
#include "Material.h"
#include "Light.h"
#include "stdlib.h"
#include <iostream>

Scene::Scene(const char* filename) :
	name(), file(filename)
{
}

bool Scene::Initialize() {
	if (!file.LoadFile())
	{
		std::cerr << "ERROR: Failed to load scene file! Either:" << std::endl
				<< "\t1) The default scene file is not found." << std::endl
				<< "\t2) The scene file you specified sucks for some reason." << std::endl;
		return false;
	}

	TiXmlHandle fileHandle(&file);
	TiXmlElement* child = fileHandle.FirstChild("scene").ToElement();
	if (!child)
	{
		std::cerr << "ERROR: No scene name or corrupt scene file!" << std::endl;
		return false;
	}
	name = child->Attribute("name");
	std::cout << "Initializing \"" << name << "\"..." << std::endl;
	child = fileHandle.FirstChild("scene").FirstChild("renderable").FirstChild("sphere").ToElement();
	if (!child)
		std::cerr << "No spheres? :(" << std::endl;

	for(child; child; child = child->NextSiblingElement())
	{
		// do something
	}
}
