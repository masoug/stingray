/*
 * Light.h
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#ifndef LIGHT_H_
#define LIGHT_H_

#include "Object.h"
#include "Vector.h"
#include "stdlib.h"

class Light: public Object {
public:
	Light();
	bool Intersect(const Ray &ray, double &distance) {return false;};
	Vector GetNormal(const Vector &pos) const {return Vector();};

	double intensity;
};

#endif /* LIGHT_H_ */
