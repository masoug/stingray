/*
 * Material.cpp
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#include "Material.h"

Material::Material(Spectrum col, double a, double d) :
	reflects(0.0), color(col.r, col.g, col.b), ka(a), kd(d) {
}
