/*
 * main.cpp
 *
 * Main raytracer system.
 *
 *  Created on: Nov 6, 2011
 *      Author: Sammy
 */

#include "math.h"
#include <iostream>
#include <string>
#include "stdio.h"
#include "Vector.h"
#include "Ray.h"
#include "Sphere.h"
#include "Utils.h"
#include "Shader.h"
#include "Scene.h"
#include "Light.h"
#include "Spectrum.h"

using namespace std;

Spectrum Raytrace(Ray &camRay, Scene &scene, int depth) {
	const int maxDepth = 50;
	Vector poi, normal, lightVec;
	Object *currentObject = NULL;
	Light currentLight;
	double dist = 1e20;
	Spectrum color;
	for (int i = 0; i < abs(scene.objects.size()); i++) // Find closest object.
	{
		if (scene.objects.at(i)->Intersect(camRay, dist)) {
			currentObject = scene.objects.at(i);
			//std::cout << dist << std::endl;
		}
	}
	if (currentObject != NULL) {
		poi = camRay.origin + (camRay.direction * dist);
		normal = currentObject->GetNormal(poi);
		normal.Normalize();
 		// Process each light in the scene.
		for (int i = 0; i < abs(scene.lights.size()); i++)
		{
			currentLight = scene.lights.at(i);
			lightVec = currentLight.position - poi;
			lightVec.Normalize();
			Lambert(currentObject->material, lightVec, normal, color);
			LightFalloff(currentLight, poi, color);
		}
		// Process reflection with recursive methods.
		if (currentObject->material.reflects && depth < maxDepth)
		{
			Vector reflect = camRay.direction - normal * (2.0
					* camRay.direction.Dot(normal));
			reflect.Normalize();
			Ray child(poi, reflect * -1);
			color += Raytrace(child, scene, depth + 1) * currentObject->material.reflects;
		}
		return color;
	} else
		// Background?
		return Spectrum(0.0, 0.0, 0.0);
}

int main(int argc, char *argv[]) {
	const int length = 1024, width = 768;
	std::cout << "\t\tS T I N G R A Y" << std::endl;
	//	if (argc < 2)
	//	{
	//		std::cerr << "ERROR: Invalid arguments!" << std::endl
	//				<< "Usage: Raytracer <image.ppm>" << std::endl;
	//		return -1;
	//	}
	//FILE *image = fopen(argv[1], "w");
	FILE *image = fopen("sammy.ppm", "w");
	fprintf(image, "P3\n%d %d\n255\n", length, width);
	Scene scene("Scene.xml");
	scene.Initialize();
	Ray camRay(Vector(0.0, 0.0, -256), Vector());
	Spectrum currColor;
	for (int y = -(width / 2); y < (width / 2); y++) {
		for (int x = -(length / 2); x < (length / 2); x++) {
			camRay.direction = Vector(x, y, 256);
			camRay.direction.Normalize();
			currColor = Raytrace(camRay, scene, 0);
			currColor *= 1000;
			fprintf(image, "%d %d %d ", clamp((int) currColor.r),
					clamp((int) currColor.g), clamp((int) currColor.b));
		}
		fprintf(image, "\n");
	}
	fclose(image);
	return 0;
}
