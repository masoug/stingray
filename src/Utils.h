/*
 * Utils.h
 *
 *  Created on: Nov 9, 2011
 *      Author: Sammy
 */

#ifndef UTILS_H_
#define UTILS_H_

#include "math.h"

#define EPSILON 0.0001f

inline int clamp(double x) {
	return x < 0 ? 0 : x > 255 ? 255 : x;
}

#endif /* UTILS_H_ */
