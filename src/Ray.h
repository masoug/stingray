/*
 * Ray.h
 *
 * Ray implementation.
 *
 *  Created on: Nov 8, 2011
 *      Author: Sammy
 */

#ifndef RAY_H_
#define RAY_H_

#include "Vector.h"
#include "stdlib.h"

class Ray
{
public:
	Ray(const Vector &orig, const Vector &dir);

	Vector origin;
	Vector direction;
};

#endif /* RAY_H_ */
