/*
 * Shader.cpp
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#include "Shader.h"
#include "Utils.h"
#include <iostream>

double DistanceSqr(const Vector &start, const Vector &end)
{
	return ((end.x - start.x)*(end.x - start.x))
			+ ((end.y - start.y)*(end.y - start.y))
			+ ((end.z - start.z)*(end.z - start.z));
}

void Lambert(const Material &mat, Vector &light, Vector &normal, Spectrum &spec) {
	double lamb = light.Dot(normal);
	spec += (mat.color * lamb * mat.kd) + (mat.color * mat.ka);
}

void LightFalloff(Light &light, Vector &poi, Spectrum &spec)
{
	spec *= 1e2 * (light.intensity/sqrt(DistanceSqr(light.position, poi)));
}
