/*
 * Spectrum.h
 *
 *  Created on: Nov 30, 2011
 *      Author: Sammy
 */

#ifndef SPECTRUM_H_
#define SPECTRUM_H_

#include "stdlib.h"

class Spectrum {
public:
	Spectrum(double arr = 0.0, double gee = 0.0, double bee = 0.0);
	virtual ~Spectrum();

	void operator+=(const Spectrum &spec);
	void operator*=(const Spectrum &spec);
	void operator*=(const double constant);
	Spectrum& operator=(const Spectrum &spec);
	Spectrum operator*(const Spectrum &spec) const;
	Spectrum operator*(const double constant) const;
	Spectrum operator/(const Spectrum &spec) const;
	Spectrum operator+(const Spectrum &spec) const;
	Spectrum operator+(const double constant) const;
	Spectrum operator-(const Spectrum &spec) const;

	double r, g, b;
};

#endif /* SPECTRUM_H_ */
