/*
 * Material.h
 *
 * The place where materials are stored.
 *
 *  Created on: Nov 10, 2011
 *      Author: Sammy
 */

#ifndef MATERIAL_H_
#define MATERIAL_H_

#include "Vector.h"
#include "Spectrum.h"
#include "stdlib.h"

class Material {
public:
	Material(Spectrum col = Spectrum(), double a = 0.4, double d = 0.15);

	double reflects;
	Spectrum color;
	double ka, kd;
};

#endif /* MATERIAL_H_ */
